<!DOCTYPE HTML>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/reset.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" /> 
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-1.11.1.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/js_detect_browser.js"></script>
		<?php wp_head(); ?>
		<?php
			
			/*
				declare global to be used in other file
			*/
			global $a_gbl_post_details;
			
			/**/
			$a_gbl_post_details['i_post_id'] = '';
			$a_gbl_post_details['s_post_meta_title'] = '';
			$a_gbl_post_details['s_post_meta_keywords'] = '';
			$a_gbl_post_details['s_post_meta_description'] = '';
			$a_gbl_post_details['s_post_banner_title'] = 'Great Game Reviews';
			$a_gbl_post_details['s_post_banner_sub_title'] = '';
			$a_gbl_post_details['s_post_permalink'] = '';
			$a_gbl_post_details['s_post_title'] = 'Great Game Reviews'; // default
			$a_gbl_post_details['s_post_sub_title'] = '';
			$a_gbl_post_details['s_post_date'] = '';
			$a_gbl_post_details['s_post_content'] = '';
			$a_gbl_post_details['s_post_featured_image_large'] = '';
			
			/*
				Get Post Details
			*/
			if ( have_posts() )
			{
				the_post();
				
				/**/
				$a_gbl_post_details['i_post_id'] = get_the_ID();
				$a_gbl_post_details['s_post_permalink'] = get_permalink();
				$a_gbl_post_details['s_post_title'] = get_the_title();
				$a_gbl_post_details['s_post_date'] = get_the_date('M j, Y');
				$a_gbl_post_details['s_post_content'] = get_the_content();

				/*
					Get Featured Image
				*/
				$a_post_featured_image_large = array();
				if ( has_post_thumbnail() ) 
				{ 
					$a_post_featured_image_large = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
					
					if( isset( $a_post_featured_image_large[0] ) && !empty( $a_post_featured_image_large[0] ) )
					{
						$a_gbl_post_details['s_post_featured_image_large'] = $a_post_featured_image_large[0];
					}
				}
								
				rewind_posts();
				
				
				/*
					Get Post Simple Fields - Site Metas
				*/
				$a_sf_site_metas = simple_fields_get_post_group_values( $a_gbl_post_details['i_post_id'], "Site Metas", true, 1 );
				if( isset( $a_sf_site_metas['Meta Title'][0] ) && !empty( $a_sf_site_metas['Meta Title'][0] ) )
				{
					$a_gbl_post_details['s_post_meta_title'] = $a_sf_site_metas['Meta Title'][0];
				}
				if( isset( $a_sf_site_metas['Meta Keywords'][0] ) && !empty( $a_sf_site_metas['Meta Keywords'][0] ) )
				{
					$a_gbl_post_details['s_post_meta_keywords'] = $a_sf_site_metas['Meta Keywords'][0];
				}
				if( isset( $a_sf_site_metas['Meta Description'][0] ) && !empty( $a_sf_site_metas['Meta Description'][0] ) )
				{
					$a_gbl_post_details['s_post_meta_description'] = $a_sf_site_metas['Meta Description'][0];
				}
				
				
				/*
					Get Post Simple Fields - Article Sub Title
				*/
				$a_sf_article_sub_title = simple_fields_get_post_group_values( $a_gbl_post_details['i_post_id'], "Article Sub Title", true, 1 );
				if( isset( $a_sf_article_sub_title['Article Sub Title'][0] ) && !empty( $a_sf_article_sub_title['Article Sub Title'][0] ) )
				{
					$a_gbl_post_details['s_post_sub_title'] = $a_sf_article_sub_title['Article Sub Title'][0];
				}
				
				
				/*
					Get Post Simple Fields - Banner Info
				*/
				$a_sf_banner_info = simple_fields_get_post_group_values( $a_gbl_post_details['i_post_id'], "Banner Info", true, 1 );
				if( isset( $a_sf_banner_info['Banner Title'][0] ) && !empty( $a_sf_banner_info['Banner Title'][0] ) )
				{
					$a_gbl_post_details['s_post_banner_title'] = $a_sf_banner_info['Banner Title'][0];
				}
				if( isset( $a_sf_banner_info['Banner Sub Title'][0] ) && !empty( $a_sf_banner_info['Banner Sub Title'][0] ) )
				{
					$a_gbl_post_details['s_post_banner_sub_title'] = $a_sf_banner_info['Banner Sub Title'][0];
				}
				
				
				/*
					Get Random Post for Head Promotion - Image Rotator
				*/
				$a_featured_image_329x90 = array();
				$a_featured_image_329x90['s_permalink'] = '';
				$a_featured_image_329x90['s_image_link'] = get_bloginfo('template_directory') . '/images/random-game-post.jpg';
				$a_custom_post_type = array('games', 'news', 'guides');
				$i_random_key = array_rand($a_custom_post_type);
				$a_random_post_type_args = array(
					'post_type' => $a_custom_post_type[$i_random_key],
					'orderby' => 'rand',
					'posts_per_page' => 1
				);
				$a_random_post_type = array();
				$o_random_post_type = new WP_Query( $a_random_post_type_args );
				if( isset($o_random_post_type->post) && !empty($o_random_post_type->post) )
				{
					/*
						Get Featured Image
					*/
					$a_featured_image = array();
					$a_featured_image = '';
					if ( has_post_thumbnail( $o_random_post_type->post->ID ) ) 
					{ 
						$a_featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $o_random_post_type->post->ID ), array(329,90) );
						
						if( isset( $a_featured_image[0] ) && !empty( $a_featured_image[0] ) )
						{
							$a_featured_image_329x90['s_permalink'] = get_permalink($o_random_post_type->post->ID);
							$a_featured_image_329x90['s_image_link'] = $a_featured_image[0];
						}
					}
					else
					{
						$a_featured_image_329x90['s_permalink'] = get_permalink($o_random_post_type->post->ID);
						$a_featured_image_329x90['s_image_link'] = get_bloginfo('template_directory') . '/images/random-game-post.jpg';
					}
				}
				wp_reset_postdata();
				
				
				
			}
			else
			{
				//404
			}
		?>
		<meta name="keywords" content="<?php if( isset($a_gbl_post_details['s_post_meta_keywords']) && !empty($a_gbl_post_details['s_post_meta_keywords']) ): echo $a_gbl_post_details['s_post_meta_keywords']; endif; ?>" />
		<meta name="description" content="<?php if( isset($a_gbl_post_details['s_post_meta_description']) && !empty($a_gbl_post_details['s_post_meta_description']) ): echo $a_gbl_post_details['s_post_meta_description']; endif; ?>" />
		<title><?php if( isset($a_gbl_post_details['s_post_meta_title']) && !empty($a_gbl_post_details['s_post_meta_title']) ) { echo $a_gbl_post_details['s_post_meta_title']; } elseif( isset($a_gbl_post_details['s_post_title']) && !empty($a_gbl_post_details['s_post_title']) ) { echo $a_gbl_post_details['s_post_title']; } else { bloginfo('name'); } ?></title>
	</head>
	<body>
	
		<div id="iddiv_maindrop_1" class="">
			
			<header id="idheader_mainheader_1" class="">
				<div id="iddiv_headerdrop_1">
					<h1 id="idh1_mainbanner_1">
						<a href="<?php bloginfo('url'); ?>">
							Great Game Reviews
						</a>
					</h1>
					<nav id="idnav_mainmenu_1">
					<?php
						wp_nav_menu( array( 'theme_location' => 'menu_primary' ) );
					?>
					</nav>
				</div>
				
				<section id="idsection_headpromotions_1">
					<div id="" class="clsdiv_ga728x90holder_1">
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:inline-block;width:728px;height:90px"
							 data-ad-client="ca-pub-0539632938799639"
							 data-ad-slot="8121667305"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
					</div>
					<?php
						if( isset($a_featured_image_329x90['s_image_link']) && !empty($a_featured_image_329x90['s_image_link']) ):
					?>
					<div id="" class="clsdiv_imagerotator_1">
						<a href="<?php echo $a_featured_image_329x90['s_permalink']; ?>">
							<img src="<?php echo $a_featured_image_329x90['s_image_link']; ?>" />
						</a>
					</div>
					<?php
						endif;
					?>
				</section>
				
				<div class="clearfix"></div>
				
			</header>