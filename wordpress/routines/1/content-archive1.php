<?php
/**
 * Template Part Archive List - 1
 *
 */
?>
			<?php
				/*
					declare global to be used in other file
				*/
				global $a_gbl_post_details;
								
				
				/*
					archive loop
				*/
				global $wp_query;
				$a_archive_post = array();
				$s_archive_pagination = '';
				if( have_posts() )
				{
					
					while( have_posts() ):
						the_post();
						
						$a_archive_post_details_template = array();
						$a_archive_post_details_template['s_the_title'] = get_the_title();
						$a_archive_post_details_template['s_permalink'] = get_permalink( get_the_id() );

						if ( has_post_thumbnail() ) 
						{
							$a_featured_image_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($o_article_related_games_details->ID), 'large' );
							
							if( isset( $a_featured_image_thumbnail[0] ) && !empty( $a_featured_image_thumbnail[0] ) )
							{
								$a_archive_post_details_template['s_featured_image_thumbnail'] = $a_featured_image_thumbnail[0];
							}
						}
						else
						{
							$a_archive_post_details_template['s_featured_image_thumbnail'] = get_bloginfo('template_directory') . '/images/img-336x280-1.jpg';
						}

						array_push($a_archive_post, $a_archive_post_details_template);

					endwhile;
					rewind_posts();
					
					
					/*
						Pagination
					*/
					$i_unlikely_integer = 999999999; // need an unlikely integer
					$a_pagination_args = array();
					$a_pagination_args = array(
										'base'         => str_replace( $i_unlikely_integer, '%#%', esc_url( get_pagenum_link( $i_unlikely_integer ) ) ),
										'format'       => '?paged=%#%',
										'current'      => max( 1, get_query_var('paged') ),
										'total'        => $wp_query->max_num_pages,
										'prev_next'    => True,
										'prev_text'    => __('Previous Page'),
										'next_text'    => __('Next Page'),
										'type' => 'list',
										'add_fragment' => '',
										'before_page_number' => '',
										'after_page_number' => ''
									);
					$s_archive_pagination = paginate_links( $a_pagination_args );
					
					
				}
				else
				{
					// 404
				}
				
				

			?>



				<section id="" class="clssection_gamelist_1">
					<header>
						<?php
							if( isset($a_gbl_post_details['s_post_title']) && !empty($a_gbl_post_details['s_post_title']) ):
						?>
						
						<h1>
							<a href="<?php if( isset($a_gbl_post_details['s_permalink']) && !empty($a_gbl_post_details['s_permalink']) ) { echo $a_gbl_post_details['s_permalink']; } ?>							">
								<?php echo strtoupper( $a_gbl_post_details['s_post_title'] ); ?>
							</a>
						</h1>
						<?php
							endif;
						?>
					</header>
					
					<?php
						if( isset( $a_gbl_post_details['s_post_content'] ) && !empty( $a_gbl_post_details['s_post_content'] ) )
						{
							echo $a_gbl_post_details['s_post_content'];
						}
					?>

					<?php
						if( isset($a_archive_post) && !empty($a_archive_post) ):
					?>
						
						<?php
							foreach( $a_archive_post AS $a_archive_post_details ):
						?>
						
						<article class="clsarticle_game_1">
							<div class="clsdiv_gameimage_1">
								<a href="<?php echo $a_archive_post_details['s_permalink']; ?>">
									<img src="<?php echo $a_archive_post_details['s_featured_image_thumbnail']; ?>" />
								</a>
							</div>
							<h2>
								<a href="<?php echo $a_archive_post_details['s_permalink']; ?>">
									<?php echo $a_archive_post_details['s_the_title']; ?>
								</a>
							</h2>
						</article>
						
						<?php
							endforeach;
						?>
						
						
					<?php
						else:
					?>
					<p>
						No Post Yet.
					</p>					
					<?php
						endif;
					?>
					
					<div class="clearfix"></div>
					
					<?php
						if( isset($s_archive_pagination) && !empty($s_archive_pagination) ):
					?>
					
					<nav id="" class="clsnav_gamelistpaging_1">
						
						<?php
							echo $s_archive_pagination;
						?>
						
					</nav>
					
					<?php
						endif;
					?>
	
		
					<div class="clearfix"></div>
					
					
				</section>	
				
				