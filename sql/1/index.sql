-- JOIN - return rows when there is at least one match in both tables
-- INNER JOIN - return rows when there is at least one match in both tables. Same as JOIN
-- LEFT JOIN - return all rows from the left table even if there are no matches in the right table
-- RIGH JOIN - return all rows from the right table even if there are no matches in the left table
-- FULL JOIN - return rows when there is a match in one of the tables
-- Drop Table - is used to delete a table
-- DROP DATABASE - is used to delete a database
-- TRUNCATE TABLE - deletes data inside the table
-- DELETE - uses to delete rows in a table

SELECT 
	column_name,column_name
FROM 
	table_name;

SELECT DISTINCT 
	column_name,column_name
FROM 
	table_name;
	
SELECT 
	column_name,column_name
FROM 
	table_name
WHERE 
	column_name operator value;

SELECT 
	column_name,column_name
FROM 
	table_name
ORDER BY 
	column_name,column_name ASC|DESC;
	
INSERT INTO 
	table_name (column1,column2,column3,...)
VALUES 
	(value1,value2,value3,...);
	
UPDATE 
	table_name
SET 
	column1=value1,column2=value2,...
WHERE 
	some_column=some_value;
	
DELETE FROM 
	table_name
WHERE 
	some_column=some_value;
	
SELECT TOP 
	number|percent column_name(s)
FROM 
	table_name;	
	
SELECT 
	column_name(s)
FROM 
	table_name
WHERE 
	column_name 
LIKE 
	pattern;
	
SELECT 
	column_name(s)
FROM 
	table_name
WHERE 
	column_name 
	IN 
	(value1,value2,...);
	
SELECT 
	column_name(s)
FROM 
	table_name
WHERE 
	column_name 
	BETWEEN value1 AND value2;
	
SELECT 
	column_name AS alias_name
FROM 
	table_name AS alias_name;
	
SELECT 
	column_name(s)
FROM 
	table1
INNER JOIN 
	table2
	ON table1.column_name=table2.column_name;

-- OR PS! INNER JOIN is the same as JOIN.
	
SELECT 
	column_name(s)
FROM 
	table1
JOIN 
	table2
	ON table1.column_name=table2.column_name;
	
SELECT 
	column_name(s)
FROM 
	table1
LEFT JOIN 
	table2
	ON table1.column_name=table2.column_name;
	
SELECT 
	column_name(s)
FROM 
	table1
RIGHT JOIN 
	table2
	ON table1.column_name=table2.column_name;
	
SELECT 
	column_name(s)
FROM 
	table1
FULL OUTER JOIN 
	table2
	ON table1.column_name=table2.column_name;
	
SELECT 
	column_name(s) 
FROM 
	table1
UNION
SELECT 
	column_name(s) 
FROM 
	table2;

CREATE DATABASE dbname;

CREATE TABLE table_name
(
column_name1 data_type(size),
column_name2 data_type(size),
column_name3 data_type(size),
....
);

DROP TABLE table_name

DROP DATABASE database_name

TRUNCATE TABLE table_name

