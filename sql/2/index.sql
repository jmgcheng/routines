SELECT 
	* 
FROM 
	Customers 
WHERE 
	lastname = 'cheng'
	AND
	( age >= 5 OR age <= 20 )
ORDER BY
	id ASC
LIMIT
	10
OFFSET
	0

SELECT 
	* 
FROM 
	Customers 
WHERE 
	age IN (5, 20)
	
UPDATE 
	table_name
SET 
	column1=value1,
	column2=value2
WHERE 
	some_column=some_value;
	
DELETE FROM 
	table_name
WHERE 
	some_column=some_value;

	