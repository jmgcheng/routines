/** cakephpv3 lesson {n} - notes */
	- cakePHP is convention over configuration. Follow their convention properly to avoid configuration
		- eg. naming conventions
	- Naming conventions are very important in CakePHP. By naming our Table object ArticlesTable, CakePHP can automatically infer that this Table object will be used in the ArticlesController, and will be tied to a database table called articles
	- CakePHP will dynamically create a model object for you if it cannot find a corresponding file in src/Model/Table. This also means that if you accidentally name your file wrong (i.e. articlestable.php or ArticleTable.php), CakePHP will not recognize any of your settings and will use the a generated model instead.
		
/** cakephpv3 lesson {n} - conventions */
	- Database table: “people”
	- Table class: “PeopleTable”, found at src/Model/Table/PeopleTable.php
	- Entity class: “Person”, found at src/Model/Entity/Person.php
	- Controller class: “PeopleController”, found at src/Controller/PeopleController.php
	- View template, found at src/Template/People/index.ctp	
	// http://book.cakephp.org/3.0/en/intro/conventions.html		

/** cakephpv3 lesson {n} - install/setup */
	- composer should already be intalled
	- inside htdocs. Right click then 'use composer here'
	- cmd will run
	composer create-project --prefer-dist cakephp/app cakesetup
	- ..composer will do all the work..
	//
	http://localhost/cakesetup/
	
/** cakephpv3 lesson {n} - Development Server */
	//if you don't want to use xampp?
	- run cmd
		cd to your project folder
		bin\cake server
	//default
	http://localhost:8765/
	
/** cakephpv3 lesson {n} - Database Connection */	
	- configure at config/app.php
	
/** cakephpv3 lesson {n} - basic mvc structure */
	//database table
	articles
	//models
	src/model/table/ArticlesTable.php
	//controllers
	src/controller/ArticlesController.php
	//views
	src/Template/Articles/index.ctp

/** cakephpv3 lesson {n} - Generating Scaffold Code */
	This will generate the controllers, models, views, their corresponding test cases, and fixtures
	//users, bookmakrs, ang tags here are the tables in phpAdmin it is connected to
	bin\cake bake all users
	bin\cake bake all bookmarks
	bin\cake bake all tags
	//
	bin\cake bake model Categories
	bin\cake bake controller Categories
	bin\cake bake template Categories
	//
	http://localhost/cakesetup/bookmarks
	http://localhost:8765/bookmarks

/** cakephpv3 lesson {n} - Adding Password Hashing */
	//inside the model/entity
	use Cake\Auth\DefaultPasswordHasher;
	protected function _setPassword($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }

/** cakephpv3 lesson {n} - App Controller */
	- AppController class is the parent class to all of your application’s controllers.
	- if you want a functionality that triggers on every controller you created then might as well put it at AppController
	
/** cakephpv3 lesson {n} - basic Controller */
	// src/Controller/ArticlesController.php
	namespace App\Controller;
	class ArticlesController extends AppController
	{
		public function index()
		{
			$articles = $this->Articles->find('all');
			$this->set(compact('articles'));
		}
	}
	
/** cakephpv3 lesson {n} - controller - actions */	
	public function view($id)
    { // Action logic goes here. }
    public function share($customerId, $recipeId)
    { // Action logic goes here. }
    public function search($query)
    { // Action logic goes here. }
	
/** cakephpv3 lesson {n} - controller - interacting views */
	// First you pass data from the controller:
	$this->set('color', 'pink');
	// Then, in the view, you can utilize the data:
	?>
	You have selected <?= h($color) ?> icing for the cake.
	$data = [
	'color' => 'pink',
	'type' => 'sugar',
	'base_price' => 23.95
	];
	// Make $color, $type, and $base_price
	// available to the view:
	$this->set($data);
	
/** cakephpv3 lesson {n} - controller - rendering view */
	public function search()
    {
        // Render the view in src/Template/Recipes/search.ctp
        $this->render();
    }
	// Render the element in src/Template/Element/ajaxreturn.ctp
	$this->render('/Element/ajaxreturn');	
	
/** cakephpv3 lesson {n} - controller - redirect */	
	// Logic for finalizing order goes here
    if ($success) {
        return $this->redirect(
            ['controller' => 'Orders', 'action' => 'thanks']
        );
    }
    return $this->redirect(
        ['controller' => 'Orders', 'action' => 'confirm']
    );
	//
	return $this->redirect('/orders/thanks');
	return $this->redirect('http://www.example.com');
	//
	return $this->redirect(['action' => 'edit', $id]);
	//
	return $this->redirect($this->referer());
	//
	return $this->redirect([
		'controller' => 'Orders',
		'action' => 'confirm',
		'?' => [
			'product' => 'pizza',
			'quantity' => 5
		],
		'#' => 'top'
	]);
	//Redirecting to Another Action on the Same Controller
	$this->setAction('index');
	
/** cakephpv3 lesson {n} - controller - loading additional models */
	$this->loadModel('Articles');
	$recentArticles = $this->Articles->find('all', [
		'limit' => 5,
		'order' => 'Articles.created DESC'
	]);

/** cakephpv3 lesson {n} - controller - paginate */	
	public $paginate = [
        'Articles' => [
            'conditions' => ['published' => 1]
        ]
    ];
	
/** cakephpv3 lesson {n} - controller - load components */
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Csrf');
		$this->loadComponent('Comments', Configure::read('Comments'));
	}
	
/** cakephpv3 lesson {n} - controller - load helpers */	
	public $helpers = ['Form'];

/** cakephpv3 lesson {n} - Components */
	//packages of logic that are shared between controllers

/** cakephpv3 lesson {n} - views - Templates */	
	- views: Templates are the part of the page that is unique to the action being run.
	- elements: small, reusable bits of view code. Elements are usually rendered inside views.
	- layouts: template files that contain presentational code that wraps many interfaces in your application.
	- helpers: these classes encapsulate view logic that is needed in many places in the view layer.
	- cells: these classes provide miniature controller-like features for creating self contained UI components.
	
/** cakephpv3 lesson {n} - views - variables */	
	//You can escape user content with the h() function:
	<?= h($user->bio); ?>
	
/** cakephpv3 lesson {n} - views - setting variables */		
	$this->set('activeMenuButton', 'posts');
	
/** cakephpv3 lesson {n} - views - extending */			
	<!-- src/Template/Common/view.ctp -->
	<h1><?= $this->fetch('title') ?></h1>
	<?= $this->fetch('content') ?>
	<div class="actions">
		<h3>Related actions</h3>
		<ul>
		<?= $this->fetch('sidebar') ?>
		</ul>
	</div>
	
/** cakephpv3 lesson {n} - views - using view blocks */	
	// Create the sidebar block.
	$this->start('sidebar');
	echo $this->element('sidebar/recent_topics');
	echo $this->element('sidebar/recent_comments');
	$this->end();
	// Append into the sidebar later on.
	$this->start('sidebar');
	echo $this->fetch('sidebar');
	echo $this->element('sidebar/popular_topics');
	$this->end();
	
/** cakephpv3 lesson {n} - views - displaying blocks */
	<?= $this->fetch('sidebar') ?>
	
/** cakephpv3 lesson {n} - views - layout */	
	//CakePHP’s default layout is located
	src/Template/Layout/default.ctp
	//Other layout files should be placed in
	src/Template/Layout
	//
	// From a controller
	public function admin_view()
	{
		// Stuff
		$this->getView()->layout('admin');
		// or the following before 3.1
		$this->layout = 'admin';
	}
	// From a view file
	$this->layout = 'loggedin';
	
/** cakephpv3 lesson {n} - migration */
	- run cmd
	//create a migration file
	bin\cake migrations create Initial
	- A migration file will be generated in the /config/Migrations folder
	- edit that file for your table data
	//create your tables
	bin\cake migrations migrate

/** cakephpv3 lesson {n} - model */
	- CakePHP’s model class files are split between Table and Entity objects
	
/** cakephpv3 lesson {n} - DB Access & ORM - quick sample */	
	use Cake\ORM\TableRegistry;
	$articles = TableRegistry::get('Articles');
	$query = $articles->find();
	foreach ($query as $row) {
		echo $row->title;
	}
	
/** cakephpv3 lesson {n} - database basics - raw select */
	use Cake\Datasource\ConnectionManager;
	$connection = ConnectionManager::get('default');
	$results = $connection->execute('SELECT * FROM articles')->fetchAll('assoc');
	//
	$results = $connection
    ->execute('SELECT * FROM articles WHERE id = :id', ['id' => 1])
    ->fetchAll('assoc');
	//
	$results = $connection
    ->execute(
        'SELECT * FROM articles WHERE created >= :created',
        ['created' => DateTime('1 day ago')],
        ['created' => 'datetime']
    )
    ->fetchAll('assoc');
	//query builder
	$results = $connection
    ->newQuery()
    ->select('*')
    ->from('articles')
    ->where(['created >' => new DateTime('1 day ago'), ['created' => 'datetime']])
    ->order(['title' => 'DESC'])
    ->execute()
    ->fetchAll('assoc');

/** cakephpv3 lesson {n} - database basics - raw insert */
	use Cake\Datasource\ConnectionManager;
	$connection = ConnectionManager::get('default');
	$connection->insert('articles', [
		'title' => 'A New Article',
		'created' => new DateTime('now')
	], ['created' => 'datetime']);

/** cakephpv3 lesson {n} - database basics - raw update */
	use Cake\Datasource\ConnectionManager;
	$connection = ConnectionManager::get('default');
	$connection->update('articles', ['title' => 'New title'], ['id' => 10]);
	//
	$stmt = $conn->query('UPDATE posts SET published = 1 WHERE id = 2');
	//
	$stmt = $conn->execute(
		'UPDATE posts SET published = ? WHERE id = ?',
		[1, 2] );
	//
	$stmt = $conn->execute(
		'UPDATE posts SET published_date = ? WHERE id = ?',
		[new DateTime('now'), 2],
		['date', 'integer']
	);

/** cakephpv3 lesson {n} - database basics - raw delete */
	use Cake\Datasource\ConnectionManager;
	$connection = ConnectionManager::get('default');
	$connection->delete('articles', ['id' => 10]);

/** cakephpv3 lesson {n} - database basics - newQuery */
	$query = $conn->newQuery();
	$query->update('posts')
		->set(['publised' => true])
		->where(['id' => 2]);
	$stmt = $query->execute();
	//
	$query = $conn->newQuery();
	$query->select('*')
		->from('posts')
		->where(['published' => true]);
	foreach ($query as $row) {
		// Do something with the row.
	}

/** cakephpv3 lesson {n} - query builder -  */
	use Cake\ORM\TableRegistry;
	$articles = TableRegistry::get('Articles');
	// Start a new query.
	$query = $articles->find();
	// Inside ArticlesController.php
	$query = $this->Articles->find();

/** cakephpv3 lesson {n} - authentication - basic */
	// In src/Controller/AppController.php
	namespace App\Controller;
	use Cake\Controller\Controller;
	class AppController extends Controller
	{
		$this->loadComponent('Flash');
		$this->loadComponent('Auth', [
			'authorize'=> 'Controller',//added this line
			'authenticate' => [
				'Form' => [
					'fields' => [
						'username' => 'email',
						'password' => 'password'
					]
				]
			],
			'loginAction' => [
				'controller' => 'Users',
				'action' => 'login'
			],
			'unauthorizedRedirect' => $this->referer()
		]);
		// Allow the display action so our pages controller
		// continues to work.
		$this->Auth->allow(['display']);
	}
	public function isAuthorized($user)
	{
		return false;
	}
	// In src/Controller/UsersController.php
	public function login()
	{
		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			if ($user) {
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error('Your username or password is incorrect.');
		}
	}
	public function logout()
	{
		$this->Flash->success('You are now logged out.');
		return $this->redirect($this->Auth->logout());
	}
	public function beforeFilter(\Cake\Event\Event $event)
	{
		$this->Auth->allow(['add']);
	}
	// src/Template/Users/login.ctp
	<h1>Login</h1>
	<?= $this->Form->create() ?>
	<?= $this->Form->input('email') ?>
	<?= $this->Form->input('password') ?>
	<?= $this->Form->button('Login') ?>
	<?= $this->Form->end() ?>
	// In src/Template/Layout/default.ctp
	// Under the existing flash message.
	<?= $this->Flash->render('auth') ?>

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

/** cakephpv3 lesson {n} -  */

