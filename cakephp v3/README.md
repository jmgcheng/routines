## problems/questions/intimidations/thoughts
* if forgot everything. What are the steps to quickly refresh my mind
* * read the naming conventions first
* * remember that cakephp is not hard but only strict to its conventions
* * its just like the CI we learned before but just have conventions
* * you have the option to bake or not
* * remember that the war starts at creating the db schema. You need to be very clear about your relationships and offcourse the conventions. If you create the schema properly, the bake console can save you a lot of time
* how does installing cakephp work
* * right now i do. Git Bash, then composer
* * since the composer is installed globally, I always use git bash
* * then i `composer create-project --prefer-dist cakephp/app [my_app_name]`
* * remember that cakephp has requirements such as latest php version. Check cakephp website for the requirements
* * I've encountered a problem at eleven2 where the site dont show anything coz of php lower version. Solved by htaccess code given by eleven2 staff
* what happens when i bake?
* * reads your table and the relationship it has with other tables
* * creates files and generate codes of controller, model entity and table, template view, test case files, and fixtures. Idk the use of the test case files and fixtures for now
* what if i dont bake? What will i do with the controllers, models, and views? Will it work easily?
* * eg. site.com/products Since cakephp is convention over configuration, by default it will look for a ProductsController.php file and gives you an error if you dont. It also gives you an error if you dont have a default method index inside that controller and another error if the corresponding view template is not found.
* * so with that url above you should have a ProductsController.php, a index action method inside, and a index.ctp view.
* * also always take note of the naming conventions so that your controller, view, model, etc would interact automatically
* if baking tables with existing files. What steps should i do?
* * its better to save your codes of controller, model entity and table, and views for reference later
* * when you bake, the console will ask you if you want to overwrite the existing files if there is any
* what happens when i dont follow the conventions?
* * dont use cakephp if your not following its conventions. It is there to assist you automatically on other stuffs you dont need to waste your time
* * if you break the conventions then you can break the "automatic stuffs"
* is cakephp flexible
* * as of now i can say yes. Just follow the conventions
* * but not as flexible as breaking the convenstions. Though what is the use of using cake if your just going to break the conventions?
* is routing hard?
* * no, its just a bunch of arrays. Just more complex than CI routing.
* is querying the database hard?
* * no, actually the query builder is pretty useful and dynamic
* * you can do almost any query using the query builder. joins, left joins, etc
* * you can also create a raw query string and pass it to query method rather than the builder but i only used it on calling stored procedures
* what does cakephp query builder cant do?
* * as of now i think its hard to create a query with a subquery
* * calling sp though you can use the query method
* is theming hard?
* * i was not able to do this yet but i think no
* * i was able to create theme for emailer and its not that hard. Just follow the naming conventions and the action methods will automatically look for its corresponding view
* what are those `use/.../...` syntax
* * just like import, require, or include. Its just how oop includes other files
* * dont worry about this. Cakephp will just show you an error if you need to include something by using a class or method
* is cakephp the best thing to use on upcoming project
* * its still 50 50 for me
* * if the db schema is to complex to handle by Wordpress then yes
* * maybe for API projects?
* problems i foresee if using cakephp in a project
* * if one of your teamates does not know how to use it, might include the owner and pm, then they might break the conventions that needed to be followed
* * breaking the conventions will cause waves of trouble that ends up with a big tsunami shake
* what does cakephp take away from me in a good thing
* * generating the base controller, model, and views is such a big help. I can say that I should have used cakephp on all of my ci projects before
* * waste time creating repeatative crud code
* what does cakephp take away from me in a bad thing
* * there is no auto generated admin page
* * you still need to assemble all the stuff like putting the wyswyg editor, attach post feature image, add gallery, etc. Same as to what I felt when creating website using CI
* how to disable the default debug bar of cakephp?
* * uncomment the plugin load at bootstrap.php


## api - problems/questions/intimidations/thoughts
* what i need to know fast to remove the forgot it so i fear syndrome
* what are the limitations of cakephp api
* what are the requirements to setup the api for cake
* what are the steps to setup the api
* what are the pitfalls to note while setting and using the api of cake
