## problems/questions/intimidations/thoughts
* what i need to know fast to remove the forgot it so i fear syndrome
** mongodb is really just like sql. But that syntax on how you use it is javascript object. Its really easy to understand
** comparing names. Database is still Database, Table is Collection, Row is Document, Column is Field, Join is Embedded Documents
* what mongodb can do really
** just like what sql can do but it has its own way that's why they differ in advantages and disadvantages
* what are the limitations of mongodb
** idk yet
* what are the requirements to setup mongodb
** you just need to download it at their website then run mongod and mongo
* what are the steps to setup mongodb
** you just need to download it at their website then run mongod and mongo
* what are the pitfalls to note while setting and using mongodb
** i think you really need to properly understand what you will include in your document data and how to use the power of indexing properly
* they said, what are the best situation to use mongodb
** Big Data
** Content Management and Delivery
** Mobile and Social Infrastructure
** User Data Management
** Data Hub
* for me, what are NOT the best situation to use mongodb
** idk yet
* they said, what are the advantages using mongodb 
** Schema less : MongoDB is document database in which one collection holds different different documents. Number of fields, content and size of the document can be differ from one document to another.
** Structure of a single object is clear
** No complex joins
** Deep query-ability. MongoDB supports dynamic queries on documents using a document-based query language that's nearly as powerful as SQL
** Tuning
** Ease of scale-out: MongoDB is easy to scale
** Conversion / mapping of application objects to database objects not needed
** Uses internal memory for storing the (windowed) working set, enabling faster access of data
* for me, what are the disadvantages using mongodb