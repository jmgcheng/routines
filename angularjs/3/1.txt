/** angular - codeschool {n} - route needs */
	- using ngView
	- load ngRoute library
	- import ngRoute module
	- define routes

/** angular - codeschool {n} - route - ngView */	
	- <div ng-view></div>
	
/** angular - codeschool {n} - route - load library */
	- <script src="/js/vendor/angular-route.js"></script>
	
/** angular - codeschool {n} - route - import module */	
	// app.js
	- angular.module("NoteWrangler", ['ngRoute']);
	// routes.js
	- angular.module('NoteWrangler')
		.config(function($routeProvider){});
	
/** angular - codeschool {n} - route - define */
	// add a new route definition to the $route service
	- .when(path, route);
	// when no other route definition is matched
	- .otherwise(params);
	//
	- angular.module('NoteWrangler')
		.config(function($routeProvider){
			$routeProvider.when('/notes', {
				templateUrl: '/templates/pages/notes/index.html'
			})
			.when('/users', {
				templateUrl: '/templates/pages/users/index.html'
			})
			.when('/', {
				templateUrl: '/templates/pages/notes/index.html'
			})
			.otherwise({
				redirectTo: '/'
			})
		});
	
/** angular - codeschool {n} - route - define + controller */
	// index.html
	<a ng-repeat='note in notes' ng-href='#/notes/{{note.id}}'>
	</a>
	// routes.js
	- angular.module('NoteWrangler')
		.config(function($routeProvider){
			$routeProvider.when('/notes', {
				templateUrl: '/templates/pages/notes/index.html',
				controller: 'NotesIndexController',
				controllerAs: 'indexController'
			})
			.when('/notes/:id', {
				templateUrl: 'templates/pages/notes/show.html',
				controller: 'NotesShowController',
				controllerAs: 'showController'
			})
		});
	// notes-index-controller.js
	- angular.module('NoteWrangler')
		.controller('NotesIndexController', function($http) {
			var controller = this;
			$http({method: 'GET', url: '/notes'})
				.success(function(data){
					controller.notes = data;
			});
		});
	// notes-show-controller.js
	- angular.module('NoteWrangler')
		.controller('NotesShowController', function($http, $routeParams) {
			var controller = this;
			$http({method:'GET', url:'/notes/' + $routeParams.id})
				.success(function(data){
					controller.note = data;
				})
		});
	// note-create-controller.js
	- angular.module('NoteWrangler')
		.controller('NoteCreateController', function($http) {
			var controller = this;
			this.saveNote = function(note) {
				controller.errors = null;
				$http( {method:'POST', url:'/notes', data:note} )
				.catch(function(note) {
					controller.errors = note.data.error;
				})
			};
		});
	
	
	
	
	
	
	
	
/** angular - tutorialspoint.com {n} - ng-view */
	//creates a place holder where a corresponding view
	<div ng-app = "mainApp">
	   <div ng-view></div>
	</div>

/** angular - tutorialspoint.com {n} - ng-template */
	//used to create an html view using script tag. It contains "id" attribute which is used by $routeProvider to map a view with a controller.
	<div ng-app = "mainApp">
	   ...
	   <script type = "text/ng-template" id = "addStudent.htm">
		  <h2> Add Student </h2>
		  {{message}}
	   </script>
	</div>

/** angular - tutorialspoint.com {n} - $routeProvider */
	//is the key service which set the configuration of urls, map them with the corresponding html page or ng-template, and attach a controller with the same.
	//
	<div ng-app = "mainApp">
	   ...
	   <script type = "text/ng-template" id = "addStudent.htm">
		  <h2> Add Student </h2>
		  {{message}}
	   </script>
	</div>
	//
	var mainApp = angular.module("mainApp", ['ngRoute']);
	mainApp.config(['$routeProvider', function($routeProvider) {
	   $routeProvider.
	   when('/addStudent', {
		  templateUrl: 'addStudent.htm', controller: 'AddStudentController'
	   }).
	   when('/viewStudents', {
		  templateUrl: 'viewStudents.htm', controller: 'ViewStudentsController'
	   }).
	   otherwise({
		  redirectTo: '/addStudent'
	   });
	}]);


/** angular - tutorialspoint.com {n} - services  */
	/*
		AngularJS supports the concepts of "Separation of Concerns" using services architecture. 
		Services are javascript functions and are responsible to do a specific tasks only. 
		Controllers, filters can call them as on requirement basis. 
		Services are normally injected using dependency injection mechanism of AngularJS.
		AngularJS provides many inbuilt services for example, $http, $route, $window, $location etc. 
		Each service is responsible for a specific task for example, $http is used to make ajax call to get the server data. 
		$route is used to define the routing information and so on. 
		Inbuilt services are always prefixed with $ symbol.
	*/

/** angular - tutorialspoint.com {n} - 2 ways to create a service */
	- factory
		var mainApp = angular.module("mainApp", []);
		mainApp.factory('MathService', function() {
		   var factory = {};
		   factory.multiply = function(a, b) {
			  return a * b
		   }
		   return factory;
		});
	- service
         mainApp.service('CalcService', function(MathService){
            this.square = function(a) {
               return MathService.multiply(a,a);
            }
         });
          mainApp.controller('CalcController', function($scope, CalcService) {
            $scope.square = function() {
               $scope.result = CalcService.square($scope.number);
            }
         });
		 
/** angular - tutorialspoint.com {n} - Dependency Injection - DI */
	/*
		a software design pattern in which components are given their dependencies instead of hard coding them within the component. 
		This relieves a component from locating the dependency and makes dependencies configurable. 
		This helps in making components reusable, maintainable and testable.
	*/
	
/** angular - tutorialspoint.com {n} - angular DI */
	- value
		/*
			a javascript object and it is used to pass values to controller during config phase.
		*/
		//define a module
		var mainApp = angular.module("mainApp", []);
		//create a value object as "defaultInput" and pass it a data.
		mainApp.value("defaultInput", 5);
		...
		//inject the value in the controller using its name "defaultInput"
		mainApp.controller('CalcController', function($scope, CalcService, defaultInput) {
		   $scope.number = defaultInput;
		   $scope.result = CalcService.square($scope.number);
		   $scope.square = function() {
			  $scope.result = CalcService.square($scope.number);
		   }
		});
	- factory
		/*
			a function which is used to return value.
		*/
		//define a module
		var mainApp = angular.module("mainApp", []);
		//create a factory "MathService" which provides a method multiply to return multiplication of two numbers
		mainApp.factory('MathService', function() {
		   var factory = {};
		   factory.multiply = function(a, b) {
			  return a * b
		   }
		   return factory;
		}); 
		//inject the factory "MathService" in a service to utilize the multiply method of factory.
		mainApp.service('CalcService', function(MathService){
		   this.square = function(a) {
			  return MathService.multiply(a,a);
		   }
		});
		...
	- service
		/*
			a singleton javascript object containing a set of functions to perform certain tasks.
		*/
		//define a module
		var mainApp = angular.module("mainApp", []);
		...
		//create a service which defines a method square to return square of a number.
		mainApp.service('CalcService', function(MathService){
		   this.square = function(a) {
			  return MathService.multiply(a,a); 
		   }
		});
		//inject the service "CalcService" into the controller
		mainApp.controller('CalcController', function($scope, CalcService, defaultInput) {
		   $scope.number = defaultInput;
		   $scope.result = CalcService.square($scope.number);
		   $scope.square = function() {
			  $scope.result = CalcService.square($scope.number);
		   }
		});
	- provider
		/*
			provider is used by AngularJS internally to create services, factory etc. 
			during config phase(phase during which AngularJS bootstraps itself).
		*/
		//define a module
		var mainApp = angular.module("mainApp", []);
		...
		//create a service using provider which defines a method square to return square of a number.
		mainApp.config(function($provide) {
		   $provide.provider('MathService', function() {
			  this.$get = function() {
				 var factory = {};  
				 factory.multiply = function(a, b) {
					return a * b; 
				 }
				 return factory;
			  };
		   });
		});
	- constant
		/*
			are used to pass values at config phase considering the fact that value can not be used to be passed during config phase.
		*/
		mainApp.constant("configParam", "constant value");


/** angular - tutorialspoint.com {n} - custom directives */
	/*
		Custom directives are used in AngularJS to extend the functionality of HTML. 
		Custom directives are defined using "directive" function. 
		A custom directive simply replaces the element for which it is activated.
	*/
	Element directives − Directive activates when a matching element is encountered.
	Attribute − Directive activates when a matching attribute is encountered.
	CSS − Directive activates when a matching css style is encountered.
	Comment − Directive activates when a matching comment is encountered.

/** angular - tutorialspoint.com {n} - custom directives - element*/
	//
	<student name = "Mahesh"></student><br/>
	<student name = "Piyush"></student>
	//
	var mainApp = angular.module("mainApp", []);
	//Create a directive, first parameter is the html element to be attached.	  
	//We are attaching student html tag. 
	//This directive will be activated as soon as any student element is encountered in html
	mainApp.directive('student', function() {
	   //define the directive object
	   var directive = {};
	   //restrict = E, signifies that directive is Element directive
	   directive.restrict = 'E';
	   //template replaces the complete element with its text. 
	   directive.template = "Student: <b>{{student.name}}</b> , Roll No: <b>{{student.rollno}}</b>";
	   //scope is used to distinguish each student element based on criteria.
	   directive.scope = {
		  student : "=name"
	   }
	   //compile is called during application initialization. AngularJS calls it once when html page is loaded.
	   directive.compile = function(element, attributes) {
		  element.css("border", "1px solid #cccccc");
		  //linkFunction is linked with each element with scope to get the element specific data.
		  var linkFunction = function($scope, element, attributes) {
			 element.html("Student: <b>"+$scope.student.name +"</b> , Roll No: <b>"+$scope.student.rollno+"</b><br/>");
			 element.css("background-color", "#ff00ff");
		  }
		  return linkFunction;
	   }
	   return directive;
	});
	//
	mainApp.controller('StudentController', function($scope) {
		$scope.Mahesh = {};
		$scope.Mahesh.name = "Mahesh Parashar";
		$scope.Mahesh.rollno  = 1;
		$scope.Piyush = {};
		$scope.Piyush.name = "Piyush Parashar";
		$scope.Piyush.rollno  = 2;
	});


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


/** angular - tutorialspoint.com {n} - install */


