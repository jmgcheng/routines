/** PHP MongoDB lesson {n} - mongo - install/setup */
	//download &install mongodb
	https://www.mongodb.org/
	//run mongod & mongo
	- cmd
		e:Program Files\MongoDB\Server\3.0\bin\dir\mongod
	- create folder for the data
	- cmd
		e:Program Files\MongoDB\Server\3.0\bin\dir\mongo

/** PHP MongoDB lesson {n} - mongo - php driver */
	//dl driver correct to phpinfo
	https://s3.amazonaws.com/drivers.mongodb.org/php/index.html
	- select the right .dll
	- place the .dll in php/ext
	- place the libsasl.dll where httpd.exe is
	- update php.ini
		extension=php_mongo.dll
	- restart apache

/** PHP MongoDB lesson {n} - connection */
	<?php
	// connects to localhost:27017 //seen on cmd
	$connection = new MongoClient(); 
	// connect to a remote host (default port: 27017)
	$connection = new MongoClient( "mongodb://example.com" ); 
	// connect to a remote host at a given port
	$connection = new MongoClient( "mongodb://example.com:65432" ); 
	?>

/** PHP MongoDB lesson {n} - select database */
	$connection = new MongoClient();
	$db = $connection->dbname;

/** PHP MongoDB lesson {n} - Getting A Collection */
	$connection = new MongoClient();
	$db = $connection->baz;
	// select a collection:
	$collection = $db->foobar;
	// or, directly selecting a database and collection:
	$collection = $connection->baz->foobar;

/** PHP MongoDB lesson {n} - Inserting a Document */
	$doc = array(
	"name" => "MongoDB",
	"type" => "database",
	"count" => 1,
	"info" => (object)array( "x" => 203, "y" => 102),
	"versions" => array("0.9.7", "0.9.8", "0.9.9")
	);
	$connection = new MongoClient();
	$collection = $connection->database->collectionName;
	$collection->insert( $doc );	

/** PHP MongoDB lesson {n} - findOne */
	$connection = new MongoClient();
	$collection = $connection->database->collectionName;
	$document = $collection->findOne();
	var_dump( $document );

/** PHP MongoDB lesson {n} - Adding Multiple Documents */
	$connection = new MongoClient();
	$collection = $connection->database->collectionName;
	for ( $i = 0; $i < 100; $i++ )
	{
	$collection->insert( array( 'i' => $i, "field{$i}" => $i * 2 ) );
	}

/** PHP MongoDB lesson {n} - Counting Documents in A Collection - count */
	$connection = new MongoClient();
	$collection = $connection->database->collectionName;
	echo $collection->count();

/** PHP MongoDB lesson {n} - Using a Cursor to Get All of the Documents - find */
	$connection = new MongoClient();
	$collection = $connection->database->collectionName;
	$cursor = $collection->find();
	foreach ( $cursor as $id => $value )
	{
	echo "$id: ";
	var_dump( $value );
	}

/** PHP MongoDB lesson {n} - Setting Criteria for a Query - find */
	$connection = new MongoClient();
	$collection = $connection->database->collectionName;
	$query = array( 'i' => 71 );
	$cursor = $collection->find( $query );
	while ( $cursor->hasNext() )
	{
	var_dump( $cursor->getNext() );
	}
	//
	$query = array( "i" => array( '$gt' => 50 ) );
	$cursor = $collection->find( $query );
	//
	$query = array( 'i' => array( '$gt' => 20, "\$lte" => 30 ) );
	$cursor = $collection->find( $query );

/** PHP MongoDB lesson {n} - Creating An Index */
	$connection = new MongoClient();
	$collection = $connection->database->collectionName;
	$collection->ensureIndex( array( "i" => 1 ) );  // create index on "i"
	$collection->ensureIndex( array( "i" => -1, "j" => 1 ) );  // index on "i" descending, "j" ascending

/** PHP MongoDB lesson {n} - remove */
	$collection->remove(array('title' => "Calvin and Hobbes"));

/** PHP MongoDB lesson {n} - findAndModify */
	$retval = $col->findAndModify(
	array("inprogress" => false, "name" => "Biz report"),
	array('$set' => array('inprogress' => true, "started" => new MongoDate())),
	null,
	array(
	"sort" => array("priority" => -1),
	"new" => true,
	)
	);
	var_dump($retval);

/** PHP MongoDB lesson {n} - update */
	$c->insert(array("firstname" => "Bob", "lastname" => "Jones" ));
	$newdata = array('$set' => array("address" => "1 Smith Lane"));
	$c->update(array("firstname" => "Bob"), $newdata);
	var_dump($c->findOne(array("firstname" => "Bob")));

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

/** PHP MongoDB lesson {n} -  */

