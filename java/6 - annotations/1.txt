/** java lesson {n} - Annotations */
	a form of metadata
	provide data about a program that is not part of the program itself

/** java lesson {n} - uses */
	- Information for the compiler - Annotations can be used by the compiler to detect errors or suppress warnings.
	- Compile-time and deployment-time processing - Software tools can process annotation information to generate code, XML files, and so forth.
	- Runtime processing - Some annotations are available to be examined at runtime.	

/** java lesson {n} - Format of an Annotation */
	// simplest
	@Entity
	//
	@Override
	void mySuperMethod() { ... }
	// annotation can include elements
	@Author(
	   name = "Benjamin Franklin",
	   date = "3/27/2003"
	)
	class MyClass() { ... }
	//
	@SuppressWarnings(value = "unchecked")
	void myMethod() { ... }
	// If there is just one element named value, then the name can be omitted, as in:
	@SuppressWarnings("unchecked")
	void myMethod() { ... }
	//If the annotations have the same type, then this is called a repeating annotation:
	@Author(name = "Jane Doe")
	@Author(name = "John Smith")
	class MyClass { ... }

/** java lesson {n} - Where Annotations Can Be Used */
	/*
		Annotations can be applied to declarations: declarations of classes, fields, methods, and other program elements. When used on a declaration, each annotation often appears, by convention, on its own line.
	*/
	- Class instance creation expression:
		new @Interned MyObject();
	- Type cast:
		myString = (@NonNull String) str;
	- implements clause:
		class UnmodifiableList<T> implements
			@Readonly List<@Readonly T> { ... }
	- Thrown exception declaration:
		void monitorTemperature() throws
			@Critical TemperatureException { ... }

/** java lesson {n} - Declaring an Annotation Type */
	// using comments
	public class Generation3List extends Generation2List {
	   // Author: John Doe
	   // Date: 3/17/2002
	   // Current revision: 6
	   // Last modified: 4/12/2004
	   // By: Jane Doe
	   // Reviewers: Alice, Bill, Cindy
	   // class code goes here
	}
	// using annotation
	@interface ClassPreamble {
	   String author();
	   String date();
	   int currentRevision() default 1;
	   String lastModified() default "N/A";
	   String lastModifiedBy() default "N/A";
	   // Note use of array
	   String[] reviewers();
	}
	//
	@ClassPreamble (
	   author = "John Doe",
	   date = "3/17/2002",
	   currentRevision = 6,
	   lastModified = "4/12/2004",
	   lastModifiedBy = "Jane Doe",
	   // Note array notation
	   reviewers = {"Alice", "Bob", "Cindy"}
	)
	public class Generation3List extends Generation2List {
		// class code goes here
	}
	/*
		Note: To make the information in @ClassPreamble appear in Javadoc-generated documentation, you must annotate the @ClassPreamble definition with the @Documented annotation:
	*/
	// import this to use @Documented
	import java.lang.annotation.*;
	@Documented
	@interface ClassPreamble {
		// Annotation element definitions
	}

/** java lesson {n} - Predefined Annotation Types */
	@Deprecated - indicates that the marked element is deprecated and should no longer be used
	/**
     * @deprecated
     * explanation of why it was deprecated
     */
    @Deprecated
    static void deprecatedMethod() { }
	//
	@Override - informs the compiler that the element is meant to override an element declared in a superclass
	/*
		mark method as a superclass method
		that has been overridden
	*/
	@Override 
	int overriddenMethod() { }
	//
	@SuppressWarnings - tells the compiler to suppress specific warnings that it would otherwise generate
	/*
		use a deprecated method and tell 
		compiler not to generate a warning
	*/
	@SuppressWarnings("deprecation")
	void useDeprecatedMethod() {
		// deprecation warning
		// - suppressed
		objectOne.deprecatedMethod();
	}
	//
	@SafeVarargs - asserts that the code does not perform potentially unsafe operations on its varargs parameter
	//
	@FunctionalInterface - indicates that the type declaration is intended to be a functional interface
	//
	@Retention - annotation specifies how the marked annotation is stored
	//
	@Documented - indicates that whenever the specified annotation is used those elements should be documented using the Javadoc tool
	//
	@Target - marks another annotation to restrict what kind of Java elements the annotation can be applied to
	// 
	@Inherited - indicates that the annotation type can be inherited from the super class
	//
	@Repeatable - indicates that the marked annotation can be applied more than once to the same declaration or type use

/** java lesson {n} - Type Annotations and Pluggable Type Systems */
	@NonNull String str;

/** java lesson {n} - Repeating Annotations */
	

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */
	
/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */
	
/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */

/** java lesson {n} -  */
	
	