Java Routine - 01052015

Variables
eg.
int cadence = 0;
int speed = 0;
int gear = 1;

Primitive Data Types
eg. //Default Values
Data Type	Default Value (for fields)
byte		0
short		0
int			0
long		0L
float		0.0f
double		0.0d
char		'\u0000'
String (or any object)  	null
boolean		false

Declaring a Variable to Refer to an Array
eg.
int[] anArray;
byte[] anArrayOfBytes;
short[] anArrayOfShorts;
long[] anArrayOfLongs;
float[] anArrayOfFloats;
double[] anArrayOfDoubles;
boolean[] anArrayOfBooleans;
char[] anArrayOfChars;
String[] anArrayOfStrings;

Creating, Initializing, and Accessing an Array
eg.
// create an array of integers
anArray = new int[10];
anArray[0] = 100; // initialize first element
anArray[1] = 200; // initialize second element
anArray[2] = 300; // and so forth
System.out.println("Element 1 at index 0: " + anArray[0]);
System.out.println("Element 2 at index 1: " + anArray[1]);
System.out.println("Element 3 at index 2: " + anArray[2]);
eg.
int[] anArray = { 
    100, 200, 300,
    400, 500, 600, 
    700, 800, 900, 1000
};

Multidimensional Array
eg.
class MultiDimArrayDemo {
    public static void main(String[] args) {
        String[][] names = {
            {"Mr. ", "Mrs. ", "Ms. "},
            {"Smith", "Jones"}
        };
        // Mr. Smith
        System.out.println(names[0][0] + names[1][0]);
        // Ms. Jones
        System.out.println(names[0][2] + names[1][1]);
    }
}

Simple Assignment Operator
eg.
=       Simple assignment operator

Arithmetic Operators
eg.
+       Additive operator (also used
        for String concatenation)
-       Subtraction operator
*       Multiplication operator
/       Division operator
%       Remainder operator

Unary Operators
eg.
+       Unary plus operator; indicates
        positive value (numbers are 
        positive without this, however)
-       Unary minus operator; negates
        an expression
++      Increment operator; increments
        a value by 1
--      Decrement operator; decrements
        a value by 1
!       Logical complement operator;
        inverts the value of a boolean
		
Equality and Relational Operators
eg.
==      Equal to
!=      Not equal to
>       Greater than
>=      Greater than or equal to
<       Less than
<=      Less than or equal to

Conditional Operators
eg.
&&      Conditional-AND
||      Conditional-OR
?:      Ternary (shorthand for 
        if-then-else statement)
		
Type Comparison Operator
eg.
instanceof      Compares an object to 
                a specified type 
				
Bitwise and Bit Shift Operators
eg.
~       Unary bitwise complement
<<      Signed left shift
>>      Signed right shift
>>>     Unsigned right shift
&       Bitwise AND
^       Bitwise exclusive OR
|       Bitwise inclusive OR

The if-then-else Statement
eg.
if (testscore >= 90) {
	grade = 'A';
} else if (testscore >= 80) {
	grade = 'B';
} else {
	grade = 'F';
}

The switch Statement
eg.
switch (month) {
	case 1:  monthString = "January";
			 break;
	default: monthString = "Invalid month";
			 break;
}
eg.
switch (month) {
	case 1: case 3: case 5:
	case 7: case 8: case 10:
	case 12:
		numDays = 31;
		break;
	case 4: case 6:
	default:
		System.out.println("Invalid month.");
		break;
}
eg.
switch (month.toLowerCase()) {
	case "january":
		monthNumber = 1;
		break;
	default: 
		monthNumber = 0;
		break;
}

while
eg.
while (expression) {
     statement(s)
}

do-while
eg.
do {
     statement(s)
} while (expression);

for Statement
eg.
for (initialization; termination;
     increment) {
    statement(s)
}
eg.
int[] numbers = 
	 {1,2,3,4,5,6,7,8,9,10};
for (int item : numbers) {
	System.out.println("Count is: " + item);
}

break Statement
eg.
break;

continue Statement
eg.
continue;

return Statement
eg.
return;

Class
eg.
class MyClass {
    // field, constructor, and 
    // method declarations
}
eg.
class MyClass extends MySuperClass implements YourInterface {
    // field, constructor, and
    // method declarations
}

Declaring Member Variables
eg.
public int cadence;
public int gear;
public int speed;
Access Modifiers _ Types _ Variable Names

Access Modifiers
eg. //more coming later
public modifier
	the field is accessible from all classes.
private modifier
	the field is accessible only within its own class.
	
Defining Methods
eg.
public double calculateAnswer(	double wingSpan, 
								int numberOfEngines,
								double length, 
								double grossTons ) 
{
    //do the calculation here
}

Overloading Methods
eg.
public class DataArtist {
    ...
    public void draw(String s) {
        ...
    }
    public void draw(int i) {
        ...
    }
    public void draw(double f) {
        ...
    }
    public void draw(int i, double f) {
        ...
    }
}

Creating Objects
eg.
Point originOne = new Point(23, 94);
Rectangle rectOne = new Rectangle(originOne, 100, 200);
Rectangle rectTwo = new Rectangle(50, 100);
/*
1. Declaration: 
	The code set in bold are all variable declarations 
	that associate a variable name with an object type.
2. Instantiation: 
	The new keyword is a Java operator that creates the object.
3. Initialization: 
	The new operator is followed by a call to a constructor, 
	which initializes the new object.
*/
eg.
type name;
Point originOne;

Initializing an Object
eg.
public class Point {
    public int x = 0;
    public int y = 0;
    //constructor
    public Point(int a, int b) {
        x = a;
        y = b;
    }
}
eg.
Point originOne = new Point(23, 94);

Referencing an Object's Fields
eg.
objectReference.fieldName

Calling an Object's Methods
eg.
objectReference.methodName(argumentList);
objectReference.methodName();

Using this with a Field
eg.
public class Point {
    public int x = 0;
    public int y = 0;
     //constructor
    public Point(int x, int y) {
		/*
		x = a;
        y = b;
		*/
        this.x = x;
        this.y = y;
    }
}

Using this with a Constructor
eg.
public class Rectangle {
    private int x, y;
    private int width, height;
        
    public Rectangle() {
        this(0, 0, 1, 1);
    }
    public Rectangle(int width, int height) {
        this(0, 0, width, height);
    }
    public Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}

Controlling Access to Members of a Class
eg.
Access Levels
Modifier	Class	Package	Subclass	World
public		Y  Y  Y  Y
protected	Y  Y  Y  N
no modifier	Y  Y  N  N
private		Y  N  N  N
Visibility
Modifier	Alpha	Beta	Alphasub	Gamma
public		Y  Y  Y  Y
protected	Y  Y  Y  N
no modifier	Y  Y  N  N
private		Y  N  N  N

Static Initialization Blocks
eg.
/*
A static initialization block is a normal block of code 
enclosed in braces, { }, and preceded by the static keyword
*/
public static varType myVar = initializeClassVariable();
private static varType initializeClassVariable() {}

Nested Classes
eg.
class OuterClass {
    class NestedClass {
    }
}
eg.
class OuterClass {
    static class StaticNestedClass {
    }
    class InnerClass {
     }
}
eg. Accessed
OuterClass.StaticNestedClass
eg. Accessed
OuterClass.StaticNestedClass nestedObject =
     new OuterClass.StaticNestedClass();
	 
Shadowing
eg.
public class ShadowTest {
    public int x = 0;
    class FirstLevel {
        public int x = 1;
        void methodInFirstLevel(int x) {
            System.out.println("x = " + x);
            System.out.println("this.x = " + this.x);
            System.out.println("ShadowTest.this.x = " + ShadowTest.this.x);
        }
    }
    public static void main(String... args) {
        ShadowTest st = new ShadowTest();
        ShadowTest.FirstLevel fl = st.new FirstLevel();
        fl.methodInFirstLevel(23);
    }
}
/*
x = 23
this.x = 1
ShadowTest.this.x = 0
*/

When to Use 
	Nested Classes, 
	Local Classes, 
	Anonymous Classes, 
	and Lambda Expressions
/*
knowledge still not capable. Refer to this link someday
http://docs.oracle.com/javase/tutorial/java/javaOO/whentouse.html
*/

Enum Types
eg.
/*
An enum type is a special data type 
that enables for a variable to be a set of predefined constants. 
The variable must be equal to one of the values 
that have been predefined for it.
*/ 
public enum Day {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
    THURSDAY, FRIDAY, SATURDAY 
}
eg.
public class EnumTest {
    Day day;
    public EnumTest(Day day) {
        this.day = day;
    }
    public void tellItLikeItIs() {
        switch (day) {
            case SATURDAY: case SUNDAY:
                System.out.println("Weekends are best.");
                break;
             default:
                System.out.println("Midweek days are so-so.");
                break;
        }
    }
     public static void main(String[] args) {
        EnumTest firstDay = new EnumTest(Day.MONDAY);
        firstDay.tellItLikeItIs();
        EnumTest thirdDay = new EnumTest(Day.WEDNESDAY);
        thirdDay.tellItLikeItIs();
        EnumTest fifthDay = new EnumTest(Day.FRIDAY);
        fifthDay.tellItLikeItIs();
        EnumTest sixthDay = new EnumTest(Day.SATURDAY);
        sixthDay.tellItLikeItIs();
        EnumTest seventhDay = new EnumTest(Day.SUNDAY);
        seventhDay.tellItLikeItIs();
    }
}

Annotations
eg.
@Entity
eg.
@Override
void mySuperMethod() { ... }
eg.
@Author(
   name = "Benjamin Franklin",
   date = "3/27/2003"
)
class MyClass() { ... }
eg.
@SuppressWarnings(value = "unchecked")
void myMethod() { ... }
@SuppressWarnings("unchecked")
void myMethod() { ... }
@Author(name = "Jane Doe")
@EBook
class MyClass { ... }
@Author(name = "Jane Doe")
@Author(name = "John Smith")
class MyClass { ... }

Defining an Interface
eg.
public interface GroupedInterface extends Interface1, Interface2, Interface3 {
	// constant declarations
	// base of natural logarithms
	double E = 2.718282;
	// method signatures
	void doSomething (int i, double x);
	int doSomethingElse(String s);
}
// continue studying this http://docs.oracle.com/javase/tutorial/java/IandI/index.html












