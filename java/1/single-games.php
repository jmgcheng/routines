<?php get_header(); ?>

			<?php
				/*
					declare global to be used in other file
				*/
				global $a_gbl_post_details;
				
				
				/*
					Get Article Game Details
				*/
				if ( have_posts() )
				{
					
					/*
						Get Article Game Genres
					*/
					$o_tax_genres = array();
					$s_tax_genres = '';
					$o_tax_genres = wp_get_post_terms( $a_gbl_post_details['i_post_id'], 'genres' );
					if( isset($o_tax_genres) && !empty($o_tax_genres) )
					{
						foreach( $o_tax_genres AS $o_tax_genres_details )
						{
							if( isset($o_tax_genres_details->name) && !empty($o_tax_genres_details->name) )
							{
								if( isset($s_tax_genres) && !empty($s_tax_genres) )
								{
									$s_tax_genres = $s_tax_genres . ', ';
								}
								$s_tax_genres = $s_tax_genres . $o_tax_genres_details->name;
							}
						}
					}
					
					/*
						Get Article Game Platforms
					*/
					$o_tax_platforms = array();
					$s_tax_platforms = '';
					$o_tax_platforms = wp_get_post_terms( $a_gbl_post_details['i_post_id'], 'platforms' );
					if( isset($o_tax_platforms) && !empty($o_tax_platforms) )
					{
						foreach( $o_tax_platforms AS $o_tax_platforms_details )
						{
							if( isset($o_tax_platforms_details->name) && !empty($o_tax_platforms_details->name) )
							{
								if( isset($s_tax_platforms) && !empty($s_tax_platforms) )
								{
									$s_tax_platforms = $s_tax_platforms . ', ';
								}
								$s_tax_platforms = $s_tax_platforms . $o_tax_platforms_details->name;
							}
						}
					}
					
					
					/*
						Show Article Aside Ad 336x280
					*/
					$a_sf_article_showaaa336x280 = simple_fields_get_post_group_values($a_gbl_post_details['i_post_id'], "Article Aside Ad 336x280", true, 1);
					$s_sf_article_showaaa336x280 = 'show';
					$s_sf_article_positionaaa336x280 = 'bottom';
					if(			isset( $a_sf_article_showaaa336x280['Show Article Aside Ad 336x280'][0] )
							&&	!empty( $a_sf_article_showaaa336x280['Show Article Aside Ad 336x280'][0] )
					)
					{
						$s_sf_article_showaaa336x280 = $a_sf_article_showaaa336x280['Show Article Aside Ad 336x280'][0];
						
						if( isset($s_sf_article_showaaa336x280) && !empty($s_sf_article_showaaa336x280) && $s_sf_article_showaaa336x280 == 'show' )
						{
							/*
								check position
							*/
							if(			isset( $a_sf_article_showaaa336x280['Position Article Aside Ad 336x280'][0] )
									&&	!empty( $a_sf_article_showaaa336x280['Position Article Aside Ad 336x280'][0] )
							)
							{
								$s_sf_article_positionaaa336x280 = $a_sf_article_showaaa336x280['Position Article Aside Ad 336x280'][0];
							}
						}
					}
					
					
					
					/*
						Get Article Related Game Details
					*/
					$a_sf_article_related_game = simple_fields_get_post_group_values($a_gbl_post_details['i_post_id'], "Related Games", true, 1);
					$a_article_related_game_id = array();
					$a_article_related_games = array();
					if( isset($a_sf_article_related_game['Related Games']) && !empty($a_sf_article_related_game['Related Games']) )
					{
						$a_wp_query_args = array(
							'post__in' => $a_sf_article_related_game['Related Games'],
							'post_type' => 'games'
						);
						$o_article_related_games = new WP_Query( $a_wp_query_args );
						
						if( isset( $o_article_related_games->posts ) && !empty( $o_article_related_games->posts ) )
						{
							foreach( $o_article_related_games->posts AS $o_article_related_games_details )
							{

								/*
									Get Article Game Featured Image
								*/
								$a_featured_image_thumbnail = array();
								$s_featured_image_thumbnail = '';
								if ( has_post_thumbnail( $o_article_related_games_details->ID ) ) 
								{ 
									$a_featured_image_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($o_article_related_games_details->ID), 'thumbnail' );
									
									if( isset( $a_featured_image_thumbnail[0] ) && !empty( $a_featured_image_thumbnail[0] ) )
									{
										$o_article_related_games_details->s_featured_image_thumbnail = $a_featured_image_thumbnail[0];
									}
								}
								else
								{
									$o_article_related_games_details->s_featured_image_thumbnail = get_bloginfo('template_directory') . '/images/img-100x100-1.jpg';
								}
								
								/*
									Ger Article Permalink
								*/
								$o_article_related_games_details->s_permalink = get_permalink($o_article_related_games_details->ID);

								array_push( $a_article_related_games, $o_article_related_games_details );
							}
						}	
						wp_reset_postdata();
					}
					/*
					echo 'test';
					print_r( $a_article_related_games );
					echo 'test';
					*/
					
					
					/*
						Get Article Related guide Details
					*/
					$a_sf_article_related_guide = simple_fields_get_post_group_values($a_gbl_post_details['i_post_id'], "Related Guides", true, 1);
					$a_article_related_guide_id = array();
					$a_article_related_guides = array();
					if( isset($a_sf_article_related_guide['Related Guides']) && !empty($a_sf_article_related_guide['Related Guides']) )
					{
						$a_wp_query_args = array(
							'post__in' => $a_sf_article_related_guide['Related Guides'],
							'post_type' => 'guides'
						);
						$o_article_related_guides = new WP_Query( $a_wp_query_args );
						
						if( isset( $o_article_related_guides->posts ) && !empty( $o_article_related_guides->posts ) )
						{
							foreach( $o_article_related_guides->posts AS $o_article_related_guides_details )
							{

								/*
									Get Article Guide Featured Image
								*/
								$a_featured_image_thumbnail = array();
								$s_featured_image_thumbnail = '';
								if ( has_post_thumbnail( $o_article_related_guides_details->ID ) ) 
								{ 
									$a_featured_image_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $o_article_related_guides_details->ID ), 'thumbnail' );
									
									if( isset( $a_featured_image_thumbnail[0] ) && !empty( $a_featured_image_thumbnail[0] ) )
									{
										$o_article_related_guides_details->s_featured_image_thumbnail = $a_featured_image_thumbnail[0];
									}
								}
								else
								{
									$o_article_related_guides_details->s_featured_image_thumbnail = get_bloginfo('template_directory') . '/images/img-100x100-1.jpg';
								}
								
								/*
									Ger Article Permalink
								*/
								$o_article_related_guides_details->s_permalink = get_permalink($o_article_related_guides_details->ID);

								array_push( $a_article_related_guides, $o_article_related_guides_details );
							}
						}	
						wp_reset_postdata();
					}
					/*
					echo 'test';
					print_r( $a_article_related_guides );
					echo 'test';
					*/
					
					
					/*
						Get Article Related news Details
					*/
					$a_sf_article_related_news = simple_fields_get_post_group_values($a_gbl_post_details['i_post_id'], "Related News", true, 1);
					$a_article_related_news_id = array();
					$a_article_related_news = array();
					if( isset($a_sf_article_related_news['Related News']) && !empty($a_sf_article_related_news['Related News']) )
					{
						$a_wp_query_args = array(
							'post__in' => $a_sf_article_related_news['Related News'],
							'post_type' => 'news'
						);
						$o_article_related_news = new WP_Query( $a_wp_query_args );
						
						if( isset( $o_article_related_news->posts ) && !empty( $o_article_related_news->posts ) )
						{
							foreach( $o_article_related_news->posts AS $o_article_related_news_details )
							{

								/*
									Get Article news Featured Image
								*/
								$a_featured_image_thumbnail = array();
								$s_featured_image_thumbnail = '';
								if ( has_post_thumbnail( $o_article_related_news_details->ID ) ) 
								{ 
									$a_featured_image_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $o_article_related_news_details->ID ), 'thumbnail' );
									
									if( isset( $a_featured_image_thumbnail[0] ) && !empty( $a_featured_image_thumbnail[0] ) )
									{
										$o_article_related_news_details->s_featured_image_thumbnail = $a_featured_image_thumbnail[0];
									}
								}
								else
								{
									$o_article_related_news_details->s_featured_image_thumbnail = get_bloginfo('template_directory') . '/images/img-100x100-1.jpg';
								}
								
								/*
									Ger Article Permalink
								*/
								$o_article_related_news_details->s_permalink = get_permalink($o_article_related_news_details->ID);

								array_push( $a_article_related_news, $o_article_related_news_details );
							}
						}	
						wp_reset_postdata();
					}
					/*
					echo 'test';
					print_r( $a_article_related_news );
					echo 'test';
					*/

				}
				else
				{
					//404
				}
			?>
				
			<main id="" class="">
				<section id="" class="clssection_gamearticle_1">
					
					<article id="idarticle_article_1" class="">
						<header>
							<?php
								if( isset($a_gbl_post_details['s_post_title']) && !empty($a_gbl_post_details['s_post_title']) ):
							?>
							<h1>
								<a href="<?php echo $a_gbl_post_details['s_post_permalink']; ?>"><?php echo $a_gbl_post_details['s_post_title']; ?></a>
							</h1>
							<?php
								endif;
							?>
							<?php
								if( isset($a_gbl_post_details['s_post_sub_title']) && !empty($a_gbl_post_details['s_post_sub_title']) ):
							?>
							<p><?php echo $a_gbl_post_details['s_post_sub_title']; ?></p>
							<?php
								endif;
							?>
							
							<?php
								if( isset($a_gbl_post_details['s_post_date']) && !empty($a_gbl_post_details['s_post_date']) ):
							?>
							<span class="clsspan_articledate_1"><?php echo $a_gbl_post_details['s_post_date']; ?></span>
							<?php
								endif;
							?>
							
						</header>
						
						<?php
							if( isset($a_gbl_post_details['s_post_featured_image_large']) && !empty($a_gbl_post_details['s_post_featured_image_large']) ):
						?>
						<a class="clsa_articleimage_1" href="<?php echo $a_gbl_post_details['s_post_permalink']; ?>" title="<?php echo $a_gbl_post_details['s_post_title']; ?>">
							<img src="<?php echo $a_gbl_post_details['s_post_featured_image_large']; ?>" alt="<?php echo $a_gbl_post_details['s_post_title']; ?>" />
						</a>
						<?php
							endif;
						?>
						
						<?php
							if( isset($a_gbl_post_details['s_post_content']) && !empty($a_gbl_post_details['s_post_content']) ):
								echo $a_gbl_post_details['s_post_content'];
							endif;
						?>
						
					</article>
				
					<aside id="idaside_articleaside_1" class="">
						
						
						<?php
							if(			
										isset( $s_sf_article_showaaa336x280 )
									&&	!empty( $s_sf_article_showaaa336x280 )
									&&	$s_sf_article_showaaa336x280 == 'show'
									&&	isset( $s_sf_article_positionaaa336x280 )
									&&	!empty( $s_sf_article_positionaaa336x280 )
									&& 	$s_sf_article_positionaaa336x280 == 'top'
							):
						?>
						<div id="" class="clsdiv_ga336x280holder_1">
							<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<ins class="adsbygoogle"
								 style="display:inline-block;width:336px;height:280px"
								 data-ad-client="ca-pub-0539632938799639"
								 data-ad-slot="9598400505"></ins>
							<script>
							(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
						</div>
						<?php
							endif;
						?>
						

						<?php
							if( 
									(isset($s_tax_genres) && !empty($s_tax_genres)) 
								|| 	(isset($s_tax_platforms) && !empty($s_tax_platforms)) 
							):
						?>
						<section id="idsection_gameinfo_1">
							<header>
								<h3>
									Game Information
								</h3>
							</header>
							<table>
								<?php
									if( isset($s_tax_genres) && !empty($s_tax_genres) ):
								?>		
								<tr>
									<td>
										Genres
									</td>
									<td>
										<?php
											echo $s_tax_genres;
										?>
									</td>
								</tr>
								<?php
									endif;
								?>
								<?php
									if( isset($s_tax_platforms) && !empty($s_tax_platforms) ):
								?>		
								<tr>
									<td>
										Platforms
									</td>
									<td>
										<?php echo $s_tax_platforms;?>
									</td>
								</tr>
								<?php
									endif;
								?>
							</table>
						</section>
						<?php
							endif;
						?>
						
						
						<?php
							if( isset( $a_article_related_games ) && !empty( $a_article_related_games ) ):
						?>
						<section class="clssection_gamerelated_1">
							<header>
								<h3>
									Related Games
								</h3>
							</header>
							
							<?php
								foreach( $a_article_related_games AS $o_article_related_games_details ):
							?>
							<article class="clsarticle_gameportallink_1">
								<div class="clsdiv_gameportalimage_1">
									<a href="<?php echo $o_article_related_games_details->s_permalink; ?>">
										<img src="<?php echo $o_article_related_games_details->s_featured_image_thumbnail; ?>" />
									</a>
								</div>
								<h4>
									<a href="<?php echo $o_article_related_games_details->s_permalink; ?>">
										<?php 
											echo substr($o_article_related_games_details->post_title, 0,60); 
										?>	
									</a>
								</h4>
								<p>
									<?php echo substr($o_article_related_games_details->post_excerpt, 0,80) . '...'; ?>
								</p>
								<!-- 
								<a href="<?php //echo $o_article_related_games_details->s_permalink; ?>" class="clsa_gameportalreadmore_1">
									Read More
								</a>
								-->
								<div class="clearfix"></div>
							</article>
							<?php
								endforeach;
							?>
							
						</section>
						<?php
							endif;
						?>
						
						
						<?php
							if( isset( $a_article_related_guides ) && !empty( $a_article_related_guides ) ):
						?>
						<section class="clssection_gamerelated_1">
							<header>
								<h3>
									Related Guides
								</h3>
							</header>
							
							<?php
								foreach( $a_article_related_guides AS $o_article_related_guides_details ):
							?>
							
							<article class="clsarticle_gameportallink_1">
								<div class="clsdiv_gameportalimage_1">
									<a href="<?php echo $o_article_related_guides_details->s_permalink; ?>">
										<img src="<?php echo $o_article_related_guides_details->s_featured_image_thumbnail; ?>" />
									</a>
								</div>
								<h4>
									<a href="<?php echo $o_article_related_guides_details->s_permalink; ?>">
										<?php 
											echo substr($o_article_related_guides_details->post_title, 0,60); 
										?>	
									</a>
								</h4>
								<p>
									<?php echo substr($o_article_related_guides_details->post_excerpt, 0,80) . '...'; ?>
								</p>
								<!-- 
								<a href="<?php //echo $o_article_related_guides_details->s_permalink; ?>" class="clsa_gameportalreadmore_1">
									Read More
								</a>
								-->
								<div class="clearfix"></div>
							</article>
							
							<?php
								endforeach;
							?>
							
						</section>
						<?php
							endif;
						?>
						
						<?php
							if( isset( $a_article_related_news ) && !empty( $a_article_related_news ) ):
						?>
						
						<section class="clssection_gamerelated_1">
							<header>
								<h3>
									Related News
								</h3>
							</header>
							
							<?php
								foreach( $a_article_related_news AS $o_article_related_news_details ):
							?>
							
							<article class="clsarticle_gameportallink_1">
								<div class="clsdiv_gameportalimage_1">
									<a href="<?php echo $o_article_related_news_details->s_permalink; ?>">
										<img src="<?php echo $o_article_related_news_details->s_featured_image_thumbnail; ?>" />
									</a>
								</div>
								<h4>
									<a href="<?php echo $o_article_related_news_details->s_permalink; ?>">
										<?php 
											echo substr($o_article_related_news_details->post_title, 0,60); 
										?>	
									</a>
								</h4>
								<p>
									<?php echo substr($o_article_related_news_details->post_excerpt, 0,80) . '...'; ?>
								</p>
								<!-- 
								<a href="<?php //echo $o_article_related_news_details->s_permalink; ?>" class="clsa_gameportalreadmore_1">
									Read More
								</a>
								-->
								<div class="clearfix"></div>
							</article>
							
							<?php
								endforeach;
							?>
							
						</section>
						
						<?php
							endif;
						?>
						
						
						<?php
							if(			
										isset( $s_sf_article_showaaa336x280 )
									&&	!empty( $s_sf_article_showaaa336x280 )
									&&	$s_sf_article_showaaa336x280 == 'show'
									&&	isset( $s_sf_article_positionaaa336x280 )
									&&	!empty( $s_sf_article_positionaaa336x280 )
									&& 	$s_sf_article_positionaaa336x280 == 'bottom'
							):
						?>
						<div id="" class="clsdiv_ga336x280holder_1">
							<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<ins class="adsbygoogle"
								 style="display:inline-block;width:336px;height:280px"
								 data-ad-client="ca-pub-0539632938799639"
								 data-ad-slot="9598400505"></ins>
							<script>
							(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
						</div>
						<?php
							endif;
						?>
						
						
					</aside>
					
					<div class="clearfix"></div>
					
				</section>
					
			</main>
						
			<?php
				get_sidebar();
			?>
			
<?php get_footer(); ?>