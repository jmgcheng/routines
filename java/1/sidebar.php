<?php
	
	
	/*
		Show site Aside Ad 300x250
	*/
	$a_sf_site_showaaa300x250 = simple_fields_get_post_group_values(get_the_id(), "Site Aside Ad 300x250", true, 1);
	$s_sf_site_showaaa300x250 = 'show';
	$s_sf_site_positionaaa300x250 = 'bottom';
	if(			isset( $a_sf_site_showaaa300x250['Show Site Aside Ad 300x250'][0] )
			&&	!empty( $a_sf_site_showaaa300x250['Show Site Aside Ad 300x250'][0] )
	)
	{
		$s_sf_site_showaaa300x250 = $a_sf_site_showaaa300x250['Show Site Aside Ad 300x250'][0];
		
		if( isset($s_sf_site_showaaa300x250) && !empty($s_sf_site_showaaa300x250) && $s_sf_site_showaaa300x250 == 'show' )
		{
			/*
				check position
			*/
			if(			isset( $a_sf_site_showaaa300x250['Position Site Aside Ad 300x250'][0] )
					&&	!empty( $a_sf_site_showaaa300x250['Position Site Aside Ad 300x250'][0] )
			)
			{
				$s_sf_site_positionaaa300x250 = $a_sf_site_showaaa300x250['Position Site Aside Ad 300x250'][0];
			}
		}
	}
	
	
	
	/*
		Site Aside. Get Recent Game Reviews
	*/
	$a_recent_game_reviews_args = array(
		'post_type' => 'games',
		'orderby' => 'date',
		'order' => 'DESC',
		'posts_per_page' => 2
	);
	$a_recent_game_reviews = array();
	$o_recent_game_reviews = new WP_Query( $a_recent_game_reviews_args );
	if( isset( $o_recent_game_reviews->posts ) && !empty( $o_recent_game_reviews->posts ) )
	{
		foreach( $o_recent_game_reviews->posts AS $o_recent_game_reviews_details )
		{
			/*
				Get Article Featured Image
			*/
			$a_featured_image_thumbnail = array();
			$s_featured_image_thumbnail = '';
			if ( has_post_thumbnail( $o_recent_game_reviews_details->ID ) ) 
			{ 
				$a_featured_image_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $o_recent_game_reviews_details->ID ), 'thumbnail' );
				
				if( isset( $a_featured_image_thumbnail[0] ) && !empty( $a_featured_image_thumbnail[0] ) )
				{
					$o_recent_game_reviews_details->s_featured_image_thumbnail = $a_featured_image_thumbnail[0];
				}
			}
			else
			{
				$o_recent_game_reviews_details->s_featured_image_thumbnail = get_bloginfo('template_directory') . '/images/img-100x100-1.jpg';
			}
			
			/*
				Ger Article Permalink
			*/
			$o_recent_game_reviews_details->s_permalink = get_permalink($o_recent_game_reviews_details->ID);

			array_push( $a_recent_game_reviews, $o_recent_game_reviews_details );
		}
	}
	wp_reset_postdata();
	/*
	echo 'test';
	print_r( $a_recent_game_reviews );
	echo 'test';
	*/
	
	
	/*
		Site Aside. Get Recent Game Guides
	*/
	$a_recent_game_guides_args = array(
		'post_type' => 'guides',
		'orderby' => 'date',
		'order' => 'DESC',
		'posts_per_page' => 2
	);
	$a_recent_game_guides = array();
	$o_recent_game_guides = new WP_Query( $a_recent_game_guides_args );
	if( isset( $o_recent_game_guides->posts ) && !empty( $o_recent_game_guides->posts ) )
	{
		foreach( $o_recent_game_guides->posts AS $o_recent_game_guides_details )
		{
			/*
				Get Article Featured Image
			*/
			$a_featured_image_thumbnail = array();
			$s_featured_image_thumbnail = '';
			if ( has_post_thumbnail( $o_recent_game_guides_details->ID ) ) 
			{ 
				$a_featured_image_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $o_recent_game_guides_details->ID ), 'thumbnail' );
				
				if( isset( $a_featured_image_thumbnail[0] ) && !empty( $a_featured_image_thumbnail[0] ) )
				{
					$o_recent_game_guides_details->s_featured_image_thumbnail = $a_featured_image_thumbnail[0];
				}
			}
			else
			{
				$o_recent_game_guides_details->s_featured_image_thumbnail = get_bloginfo('template_directory') . '/images/img-100x100-1.jpg';
			}
			
			/*
				Ger Article Permalink
			*/
			$o_recent_game_guides_details->s_permalink = get_permalink($o_recent_game_guides_details->ID);

			array_push( $a_recent_game_guides, $o_recent_game_guides_details );
		}
	}
	wp_reset_postdata();
	/*
	echo 'test';
	print_r( $a_recent_game_guides );
	echo 'test';
	*/
	
	
	/*
		Site Aside. Get Recent Game News
	*/
	$a_recent_game_news_args = array(
		'post_type' => 'news',
		'orderby' => 'date',
		'order' => 'DESC',
		'posts_per_page' => 2
	);
	$a_recent_game_news = array();
	$o_recent_game_news = new WP_Query( $a_recent_game_news_args );
	if( isset( $o_recent_game_news->posts ) && !empty( $o_recent_game_news->posts ) )
	{
		foreach( $o_recent_game_news->posts AS $o_recent_game_news_details )
		{
			/*
				Get Article Featured Image
			*/
			$a_featured_image_thumbnail = array();
			$s_featured_image_thumbnail = '';
			if ( has_post_thumbnail( $o_recent_game_news_details->ID ) ) 
			{ 
				$a_featured_image_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $o_recent_game_news_details->ID ), 'thumbnail' );
				
				if( isset( $a_featured_image_thumbnail[0] ) && !empty( $a_featured_image_thumbnail[0] ) )
				{
					$o_recent_game_news_details->s_featured_image_thumbnail = $a_featured_image_thumbnail[0];
				}
			}
			else
			{
				$o_recent_game_news_details->s_featured_image_thumbnail = get_bloginfo('template_directory') . '/images/img-100x100-1.jpg';
			}
			
			/*
				Ger Article Permalink
			*/
			$o_recent_game_news_details->s_permalink = get_permalink($o_recent_game_news_details->ID);

			array_push( $a_recent_game_news, $o_recent_game_news_details );
		}
	}
	wp_reset_postdata();
	/*
	echo 'test';
	print_r( $a_recent_game_news );
	echo 'test';
	*/
	
	
?>			

			<aside id="" class="clsaside_sidebar_1">
				
				<?php
					if(			
								isset( $s_sf_site_showaaa300x250 )
							&&	!empty( $s_sf_site_showaaa300x250 )
							&&	$s_sf_site_showaaa300x250 == 'show'
							&&	isset( $s_sf_site_positionaaa300x250 )
							&&	!empty( $s_sf_site_positionaaa300x250 )
							&& 	$s_sf_site_positionaaa300x250 == 'top'
					):
				?>
				<div id="" class="clsdiv_ga300x250holder_1">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						 style="display:inline-block;width:300px;height:250px"
						 data-ad-client="ca-pub-0539632938799639"
						 data-ad-slot="6505333303"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
				<?php
					endif;
				?>
				
				
				<?php
					if( isset( $a_recent_game_reviews ) && !empty( $a_recent_game_reviews ) ):
				?>
				<section id="" class="clssection_related_1">
					<header>
						<h3>
							<a href="<?php echo get_post_type_archive_link('games'); ?>">
								Recent Game Reviews
							</a>
						</h3>
					</header>
					
					<?php
						foreach( $a_recent_game_reviews AS $o_recent_game_reviews_details ):
					?>
					
					<article class="clsarticle_relatedportallink_1">
						<div class="clsdiv_relatedportalimage_1">
							<a href="<?php echo $o_recent_game_reviews_details->s_permalink; ?>">
								<img src="<?php echo $o_recent_game_reviews_details->s_featured_image_thumbnail; ?>" />
							</a>
						</div>
						<h4>
							<a href="<?php echo $o_recent_game_reviews_details->s_permalink; ?>">
								<?php 
									echo substr($o_recent_game_reviews_details->post_title, 0,60); 
								?>	
							</a>
						</h4>
						<p>
							<?php echo substr($o_recent_game_reviews_details->post_excerpt, 0,80) . '...'; ?>
						</p>
						<!-- 
						<a href="<?php //echo $o_recent_game_reviews_details->s_permalink; ?>" class="clsa_relatedportalreadmore_1">
							Read More
						</a>
						-->
						<div class="clearfix"></div>
					</article>
					
					<?php
						endforeach;
					?>
					
				</section>
				<?php
					endif;
				?>
				
				
				<?php
					if( isset( $a_recent_game_guides ) && !empty( $a_recent_game_guides ) ):
				?>
				
				<section id="" class="clssection_related_1">
					<header>
						<h3>
							<a href="<?php echo get_post_type_archive_link('guides'); ?>">
								Recent Game Guides
							</a>
						</h3>
					</header>
					
					<?php
						foreach( $a_recent_game_guides AS $o_recent_game_guides_details ):
					?>
					<article class="clsarticle_relatedportallink_1">
						<div class="clsdiv_relatedportalimage_1">
							<a href="<?php echo $o_recent_game_guides_details->s_permalink; ?>">
								<img src="<?php echo $o_recent_game_guides_details->s_featured_image_thumbnail; ?>" />
							</a>
						</div>
						<h4>
							<a href="<?php echo $o_recent_game_guides_details->s_permalink; ?>">
								<?php 
									echo substr($o_recent_game_guides_details->post_title, 0,60); 
								?>	
							</a>
						</h4>
						<p>
							<?php echo substr($o_recent_game_guides_details->post_excerpt, 0,80) . '...'; ?>
						</p>
						<!-- 
						<a href="<?php //echo $o_recent_game_guides_details->s_permalink; ?>" class="clsa_relatedportalreadmore_1">
							Read More
						</a>
						-->
						<div class="clearfix"></div>
					</article>
					<?php
						endforeach;
					?>

				</section>
				<?php
					endif;
				?>
				
				
				<?php
					if( isset( $a_recent_game_news ) && !empty( $a_recent_game_news ) ):
				?>
				<section id="" class="clssection_related_1">
					<header>
						<h3>
							<a href="<?php echo get_post_type_archive_link('news'); ?>">
								Recent Game News
							</a>
						</h3>
					</header>
					
					<?php
						foreach( $a_recent_game_news AS $o_recent_game_news_details ):
					?>
					<article class="clsarticle_relatedportallink_1">
						<div class="clsdiv_relatedportalimage_1">
							<a href="<?php echo $o_recent_game_news_details->s_permalink; ?>">
								<img src="<?php echo $o_recent_game_news_details->s_featured_image_thumbnail; ?>" />
							</a>
						</div>
						<h4>
							<a href="<?php echo $o_recent_game_news_details->s_permalink; ?>">
								<?php 
									echo substr($o_recent_game_news_details->post_title, 0,60); 
								?>	
							</a>
						</h4>
						<p>
							<?php echo substr($o_recent_game_news_details->post_excerpt, 0,80) . '...'; ?>
						</p>
						<!-- 
						<a href="<?php //echo $o_recent_game_news_details->s_permalink; ?>" class="clsa_relatedportalreadmore_1">
							Read More
						</a>
						-->
						<div class="clearfix"></div>
					</article>
					<?php
						endforeach;
					?>
					
				</section>
				
				<?php
					endif;
				?>
				
				<?php
					if(			
								isset( $s_sf_site_showaaa300x250 )
							&&	!empty( $s_sf_site_showaaa300x250 )
							&&	$s_sf_site_showaaa300x250 == 'show'
							&&	isset( $s_sf_site_positionaaa300x250 )
							&&	!empty( $s_sf_site_positionaaa300x250 )
							&& 	$s_sf_site_positionaaa300x250 == 'bottom'
					):
				?>
				<div id="" class="clsdiv_ga300x250holder_1">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						 style="display:inline-block;width:300px;height:250px"
						 data-ad-client="ca-pub-0539632938799639"
						 data-ad-slot="6505333303"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
				<?php
					endif;
				?>
				
				
			</aside>
			