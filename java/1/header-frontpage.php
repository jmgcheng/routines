<!DOCTYPE HTML>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/reset.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" /> 
		<!-- 
		<script type="text/javascript" src="<?php //bloginfo('template_directory'); ?>/js/jquery-1.11.1.js"></script>
		-->
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/js_detect_browser.js"></script>
		<script>
			$(document).ready(function($){
				$("head").append('<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/fonts.css" />');
			});
		</script>
		<?php wp_head(); ?>
		<?php
			
			/*
				declare global to be used in other file
			*/
			global $a_gbl_post_details;
			
			/**/
			$a_gbl_post_details['i_post_id'] = '';
			$a_gbl_post_details['s_post_meta_title'] = '';
			$a_gbl_post_details['s_post_meta_keywords'] = '';
			$a_gbl_post_details['s_post_meta_description'] = '';
			$a_gbl_post_details['s_post_banner_title'] = 'Great Game Reviews';
			$a_gbl_post_details['s_post_banner_sub_title'] = '';
			$a_gbl_post_details['s_post_permalink'] = '';
			$a_gbl_post_details['s_post_title'] = 'Great Game Reviews'; // default
			$a_gbl_post_details['s_post_sub_title'] = '';
			$a_gbl_post_details['s_post_date'] = '';
			$a_gbl_post_details['s_post_content'] = '';
			$a_gbl_post_details['s_post_featured_image_large'] = '';
						
			/*
				Get Post Details
			*/
			if ( have_posts() )
			{
				the_post();
				
				/**/
				$a_gbl_post_details['i_post_id'] = get_the_ID();
				$a_gbl_post_details['s_post_permalink'] = get_permalink();
				$a_gbl_post_details['s_post_title'] = get_the_title();
				$a_gbl_post_details['s_post_date'] = get_the_date('M j, Y');
				$a_gbl_post_details['s_post_content'] = get_the_content();

				/*
					Get Featured Image
				*/
				$a_post_featured_image_large = array();
				if ( has_post_thumbnail() ) 
				{ 
					$a_post_featured_image_large = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
					
					if( isset( $a_post_featured_image_large[0] ) && !empty( $a_post_featured_image_large[0] ) )
					{
						$a_gbl_post_details['s_post_featured_image_large'] = $a_post_featured_image_large[0];
					}
				}
								
				rewind_posts();
				
				
				/*
					Get Post Simple Fields - Site Metas
				*/
				$a_sf_site_metas = simple_fields_get_post_group_values( $a_gbl_post_details['i_post_id'], "Site Metas", true, 1 );
				if( isset( $a_sf_site_metas['Meta Title'][0] ) && !empty( $a_sf_site_metas['Meta Title'][0] ) )
				{
					$a_gbl_post_details['s_post_meta_title'] = $a_sf_site_metas['Meta Title'][0];
				}
				if( isset( $a_sf_site_metas['Meta Keywords'][0] ) && !empty( $a_sf_site_metas['Meta Keywords'][0] ) )
				{
					$a_gbl_post_details['s_post_meta_keywords'] = $a_sf_site_metas['Meta Keywords'][0];
				}
				if( isset( $a_sf_site_metas['Meta Description'][0] ) && !empty( $a_sf_site_metas['Meta Description'][0] ) )
				{
					$a_gbl_post_details['s_post_meta_description'] = $a_sf_site_metas['Meta Description'][0];
				}
				
				
				/*
					Get Post Simple Fields - Article Sub Title
				*/
				$a_sf_article_sub_title = simple_fields_get_post_group_values( $a_gbl_post_details['i_post_id'], "Article Sub Title", true, 1 );
				if( isset( $a_sf_article_sub_title['Article Sub Title'][0] ) && !empty( $a_sf_article_sub_title['Article Sub Title'][0] ) )
				{
					$a_gbl_post_details['s_post_sub_title'] = $a_sf_article_sub_title['Article Sub Title'][0];
				}
				
				
				/*
					Get Post Simple Fields - Banner Info
				*/
				$a_sf_banner_info = simple_fields_get_post_group_values( $a_gbl_post_details['i_post_id'], "Banner Info", true, 1 );
				if( isset( $a_sf_banner_info['Banner Title'][0] ) && !empty( $a_sf_banner_info['Banner Title'][0] ) )
				{
					$a_gbl_post_details['s_post_banner_title'] = $a_sf_banner_info['Banner Title'][0];
				}
				if( isset( $a_sf_banner_info['Banner Sub Title'][0] ) && !empty( $a_sf_banner_info['Banner Sub Title'][0] ) )
				{
					$a_gbl_post_details['s_post_banner_sub_title'] = $a_sf_banner_info['Banner Sub Title'][0];
				}
				
			}
			else
			{
				//404
			}
		?>
		<meta name="keywords" content="<?php if( isset($a_gbl_post_details['s_post_meta_keywords']) && !empty($a_gbl_post_details['s_post_meta_keywords']) ): echo $a_gbl_post_details['s_post_meta_keywords']; endif; ?>" />
		<meta name="description" content="<?php if( isset($a_gbl_post_details['s_post_meta_description']) && !empty($a_gbl_post_details['s_post_meta_description']) ): echo $a_gbl_post_details['s_post_meta_description']; endif; ?>" />
		<title><?php if( isset($a_gbl_post_details['s_post_meta_title']) && !empty($a_gbl_post_details['s_post_meta_title']) ) { echo $a_gbl_post_details['s_post_meta_title']; } elseif( isset($a_gbl_post_details['s_post_title']) && !empty($a_gbl_post_details['s_post_title']) ) { echo $a_gbl_post_details['s_post_title']; } else { bloginfo('name'); } ?></title>
	</head>
	<body>
	
		<div id="iddiv_maindrop_1" class="">
			
			<header id="idheader_mainheader_2" class="">
				
				<div id="iddiv_headerdrop_2">
					
					<?php
						if( 
								( isset($a_gbl_post_details['s_post_banner_title']) && !empty($a_gbl_post_details['s_post_banner_title'])  )
							|| 	( isset($a_gbl_post_details['s_post_title']) && !empty($a_gbl_post_details['s_post_title'])  )
						):
					?>
					
					<h1 id="idh1_mainbanner_2">
						<a href="<?php bloginfo('url'); ?>">
							<?php
								if( isset($a_gbl_post_details['s_post_banner_title']) && !empty($a_gbl_post_details['s_post_banner_title']) ):
							?>
							<?php
								echo $a_gbl_post_details['s_post_banner_title'];
							?>
							<?php
								elseif( isset($a_gbl_post_details['s_post_title']) && !empty($a_gbl_post_details['s_post_title']) ):
							?>
							<?php
								echo $a_gbl_post_details['s_post_title'];
							?>
							<?php
								endif;
							?>
						</a>
					</h1>					
					
					<?php
						endif;
					?>
					
					
					<?php
						if( isset($a_gbl_post_details['s_post_banner_sub_title']) && !empty($a_gbl_post_details['s_post_banner_sub_title']) ):
					?>
					<p id="idp_mainbannersubtitle_1">
						<?php echo $a_gbl_post_details['s_post_banner_sub_title']; ?>
					</p>
					<?php
						endif;
					?>
					
				</div>
					
				<section id="idsection_headerissuedate_1">
					<p>
						<span class="red">
							issue
						</span>
						<br/>
						<?php echo date("M"); ?>
						<br/>
						<?php echo date("Y"); ?>
					</p>
				</section>

				<div class="clearfix"></div>
				
			</header>