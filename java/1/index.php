<?php get_header('frontpage'); ?>
			
			
			<?php
				global $a_gbl_post_details;

				/*
					Get Front Page Game Details
				*/
				$a_sf_article_front_page_game = simple_fields_get_post_group_values($a_gbl_post_details['i_post_id'], "Front Page Games", true, 1);
				$a_article_front_page_game_id = array();
				$a_article_front_page_games = array();
				if( isset($a_sf_article_front_page_game['Front Page Games']) && !empty($a_sf_article_front_page_game['Front Page Games']) )
				{
					$a_wp_query_args = array(
						'post__in' => $a_sf_article_front_page_game['Front Page Games'],
						'post_type' => 'games'
					);
					$o_article_front_page_games = new WP_Query( $a_wp_query_args );
					
					if( isset( $o_article_front_page_games->posts ) && !empty( $o_article_front_page_games->posts ) )
					{
						foreach( $o_article_front_page_games->posts AS $o_article_front_page_games_details )
						{

							/*
								Get Article Game Featured Image
							*/
							$a_featured_image_thumbnail = array();
							$s_featured_image_thumbnail = '';
							if ( has_post_thumbnail( $o_article_front_page_games_details->ID ) ) 
							{ 
								$a_featured_image_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($o_article_front_page_games_details->ID), 'thumbnail' );
								
								if( isset( $a_featured_image_thumbnail[0] ) && !empty( $a_featured_image_thumbnail[0] ) )
								{
									$o_article_front_page_games_details->s_featured_image_thumbnail = $a_featured_image_thumbnail[0];
								}
							}
							else
							{
								$o_article_front_page_games_details->s_featured_image_thumbnail = get_bloginfo('template_directory') . '/images/img-100x100-1.jpg';
							}
							
							/*
								Ger Article Permalink
							*/
							$o_article_front_page_games_details->s_permalink = get_permalink($o_article_front_page_games_details->ID);

							array_push( $a_article_front_page_games, $o_article_front_page_games_details );
						}
					}	
					wp_reset_postdata();
				}
				/*
				echo 'test';
				print_r( $a_article_front_page_games );
				echo 'test';
				*/
				
				
				
				/*
					Get Front Page news Details
				*/
				$a_sf_article_front_page_news = simple_fields_get_post_group_values($a_gbl_post_details['i_post_id'], "Front Page News", true, 1);
				$a_article_front_page_news_id = array();
				$a_article_front_page_news = array();
				if( isset($a_sf_article_front_page_news['Front Page News']) && !empty($a_sf_article_front_page_news['Front Page News']) )
				{
					$a_wp_query_args = array(
						'post__in' => $a_sf_article_front_page_news['Front Page News'],
						'post_type' => 'news'
					);
					$o_article_front_page_news = new WP_Query( $a_wp_query_args );
					
					if( isset( $o_article_front_page_news->posts ) && !empty( $o_article_front_page_news->posts ) )
					{
						foreach( $o_article_front_page_news->posts AS $o_article_front_page_news_details )
						{

							/*
								Get Article News Featured Image
							*/
							$a_featured_image_thumbnail = array();
							$s_featured_image_thumbnail = '';
							if ( has_post_thumbnail( $o_article_front_page_news_details->ID ) ) 
							{ 
								$a_featured_image_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($o_article_front_page_news_details->ID), 'thumbnail' );
								
								if( isset( $a_featured_image_thumbnail[0] ) && !empty( $a_featured_image_thumbnail[0] ) )
								{
									$o_article_front_page_news_details->s_featured_image_thumbnail = $a_featured_image_thumbnail[0];
								}
							}
							else
							{
								$o_article_front_page_news_details->s_featured_image_thumbnail = get_bloginfo('template_directory') . '/images/img-100x100-1.jpg';
							}
							
							/*
								Ger Article Permalink
							*/
							$o_article_front_page_news_details->s_permalink = get_permalink($o_article_front_page_news_details->ID);

							array_push( $a_article_front_page_news, $o_article_front_page_news_details );
						}
					}	
					wp_reset_postdata();
				}
				/*
				echo 'test';
				print_r( $a_article_front_page_news );
				echo 'test';
				*/
				
				
				
				/*
					Get Front Page Game Details
				*/
				$a_sf_article_front_page_guide = simple_fields_get_post_group_values($a_gbl_post_details['i_post_id'], "Front Page Guides", true, 1);
				$a_article_front_page_guide_id = array();
				$a_article_front_page_guides = array();
				if( isset($a_sf_article_front_page_guide['Front Page Guides']) && !empty($a_sf_article_front_page_guide['Front Page Guides']) )
				{
					$a_wp_query_args = array(
						'post__in' => $a_sf_article_front_page_guide['Front Page Guides'],
						'post_type' => 'guides'
					);
					$o_article_front_page_guides = new WP_Query( $a_wp_query_args );
					
					if( isset( $o_article_front_page_guides->posts ) && !empty( $o_article_front_page_guides->posts ) )
					{
						foreach( $o_article_front_page_guides->posts AS $o_article_front_page_guides_details )
						{

							/*
								Get Article guide Featured Image
							*/
							$a_featured_image_thumbnail = array();
							$s_featured_image_thumbnail = '';
							if ( has_post_thumbnail( $o_article_front_page_guides_details->ID ) ) 
							{ 
								$a_featured_image_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($o_article_front_page_guides_details->ID), 'thumbnail' );
								
								if( isset( $a_featured_image_thumbnail[0] ) && !empty( $a_featured_image_thumbnail[0] ) )
								{
									$o_article_front_page_guides_details->s_featured_image_thumbnail = $a_featured_image_thumbnail[0];
								}
							}
							else
							{
								$o_article_front_page_guides_details->s_featured_image_thumbnail = get_bloginfo('template_directory') . '/images/img-100x100-1.jpg';
							}
							
							/*
								Ger Article Permalink
							*/
							$o_article_front_page_guides_details->s_permalink = get_permalink($o_article_front_page_guides_details->ID);

							array_push( $a_article_front_page_guides, $o_article_front_page_guides_details );
						}
					}	
					wp_reset_postdata();
				}
				/*
				echo 'test';
				print_r( $a_article_front_page_guides );
				echo 'test';
				*/
				
				
			?>
			
			
			<main id="" class="">
				
				<section id="" class="clssection_frontpagearticle_1">
					<article id="idarticle_article_3" class="">
						<header>
							<?php
								if( isset($a_gbl_post_details['s_post_title']) && !empty($a_gbl_post_details['s_post_title']) ):
							?>
							<h1>
								<a href="<?php if( isset($a_gbl_post_details['s_post_permalink']) && !empty($a_gbl_post_details['s_post_permalink']) ) { echo $a_gbl_post_details['s_post_permalink']; } ?>">
									<?php
										echo $a_gbl_post_details['s_post_title'];
									?>
								</a>
							</h1>
							<?php
								endif;
							?>
							<?php
								if( isset($a_gbl_post_details['s_post_sub_title']) && !empty($a_gbl_post_details['s_post_sub_title']) ):
							?>
							<p>
								<?php
									echo $a_gbl_post_details['s_post_sub_title'];
								?>
							</p>
							<?php
								endif;
							?>
							<?php
								if( isset($a_gbl_post_details['s_post_date']) && !empty($a_gbl_post_details['s_post_date']) ):
							?>
							<span class="clsspan_articledate_3">
								<?php echo $a_gbl_post_details['s_post_date']; ?>
							</span>
							<?php
								endif;
							?>
						</header>
												
						<?php
							if( isset($a_gbl_post_details['s_post_featured_image_large']) && !empty($a_gbl_post_details['s_post_featured_image_large']) ):
						?>
						<a class="clsa_articleimage_3" href="<?php echo $a_gbl_post_details['s_post_permalink']; ?>" title="<?php echo $a_gbl_post_details['s_post_title']; ?>">
							<img src="<?php echo $a_gbl_post_details['s_post_featured_image_large']; ?>" alt="<?php echo $a_gbl_post_details['s_post_title']; ?>" />
						</a>
						<?php
							endif;
						?>						
						
						<?php
							if( isset($a_gbl_post_details['s_post_content']) && !empty($a_gbl_post_details['s_post_content']) ):
								echo $a_gbl_post_details['s_post_content'];
							endif;
						?>
												
						<div class="clearfix"></div>
						
					</article>
				</section>
				
				<div class="clearfix"></div>
				
				<?php
					if( isset( $a_article_front_page_games ) && !empty( $a_article_front_page_games ) ):
				?>
				
				<section id="" class="clssection_frontpagerelated_1">
					<header>
						<h3>
							Editors Top Games
						</h3>
					</header>
					
					<div class="clearfix"></div>
					
					<?php
						foreach( $a_article_front_page_games AS $o_article_front_page_games_details ):
					?>
					<article class="clsarticle_gameguideportallink_1">
						<div class="clsdiv_gameguideportalimage_1">
							<a href="<?php echo $o_article_front_page_games_details->s_permalink; ?>">
								<img src="<?php echo $o_article_front_page_games_details->s_featured_image_thumbnail; ?>" />
							</a>
						</div>
						<h4>
							<a href="<?php echo $o_article_front_page_games_details->s_permalink; ?>">
								<?php 
									echo substr($o_article_front_page_games_details->post_title, 0,60); 
								?>	
							</a>
						</h4>
						<p>
							<?php echo substr($o_article_front_page_games_details->post_excerpt, 0,80) . '...'; ?>
						</p>
						<div class="clearfix"></div>
					</article>
					<?php
						endforeach;
					?>
					
					<div class="clearfix"></div>
					
				</section>
				
				<div class="clearfix"></div>
				
				<?php
					endif;
				?>
				
				
				
				
				<?php
					if( isset( $a_article_front_page_news ) && !empty( $a_article_front_page_news ) ):
				?>
				
				<section id="" class="clssection_frontpagerelated_1">
					<header>
						<h3>
							Top Game News
						</h3>
					</header>
					
					<div class="clearfix"></div>
					
					<?php
						foreach( $a_article_front_page_news AS $o_article_front_page_news_details ):
					?>
					<article class="clsarticle_gameguideportallink_1">
						<div class="clsdiv_gameguideportalimage_1">
							<a href="<?php echo $o_article_front_page_news_details->s_permalink; ?>">
								<img src="<?php echo $o_article_front_page_news_details->s_featured_image_thumbnail; ?>" />
							</a>
						</div>
						<h4>
							<a href="<?php echo $o_article_front_page_news_details->s_permalink; ?>">
								<?php 
									echo substr($o_article_front_page_news_details->post_title, 0,60); 
								?>	
							</a>
						</h4>
						<p>
							<?php echo substr($o_article_front_page_news_details->post_excerpt, 0,80) . '...'; ?>
						</p>
						<div class="clearfix"></div>
					</article>
					<?php
						endforeach;
					?>
										
					<div class="clearfix"></div>
					
				</section>
				
				<div class="clearfix"></div>
				
				
				<?php
					endif;
				?>
				
				
				<?php
					if( isset( $a_article_front_page_guides ) && !empty( $a_article_front_page_guides ) ):
				?>
				
				<section id="" class="clssection_frontpagerelated_1">
					<header>
						<h3>
							Featured Game Guides
						</h3>
					</header>
					
					<div class="clearfix"></div>
					
					<?php
						foreach( $a_article_front_page_guides AS $o_article_front_page_guides_details ):
					?>
					<article class="clsarticle_gameguideportallink_1">
						<div class="clsdiv_gameguideportalimage_1">
							<a href="<?php echo $o_article_front_page_guides_details->s_permalink; ?>">
								<img src="<?php echo $o_article_front_page_guides_details->s_featured_image_thumbnail; ?>" />
							</a>
						</div>
						<h4>
							<a href="<?php echo $o_article_front_page_guides_details->s_permalink; ?>">
								<?php 
									echo substr($o_article_front_page_guides_details->post_title, 0,60); 
								?>	
							</a>
						</h4>
						<p>
							<?php echo substr($o_article_front_page_guides_details->post_excerpt, 0,80) . '...'; ?>
						</p>
						<div class="clearfix"></div>
					</article>
					<?php
						endforeach;
					?>
					
					<div class="clearfix"></div>
					
				</section>
				
				<div class="clearfix"></div>
				
				<?php
					endif;
				?>
				

			</main>
			
			<?php
				get_sidebar();
			?>			
			
<?php get_footer(); ?>