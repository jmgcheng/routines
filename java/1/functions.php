<?php
/**
 * GGR functions and definitions
 *
 * @package GGR
 * @subpackage GGR
 * @since GGR 1.0
 */	


/**
 * ggr_setup
 * init setup for ggr theme
 *
 **/
if( !function_exists( 'ggr_setup' ) ):
	function ggr_setup() 
	{
		/*
			Custom Menu
		*/
		register_nav_menus( array(
			'menu_primary' => 'GGR Main Menu Header'
		) );
		
		
		/*
			add_theme_support( 'post-thumbnails' );
		*/
		add_theme_support( 'post-thumbnails', array( 'games', 'news', 'guides', 'page', 'post' ) );
		
		
		/**/
		add_image_size( 'image329x90', 329, 90, true );
		
				
		/*
			Custom Post Type. Games
		*/
		$a_post_type_game_labels = array(
			'name'               => _x('Games', 'post type general name'),
			'singular_name'      => _x('Game', 'post type singular name'),
			'menu_name'          => _x('Games', 'admin menu'),
			'name_admin_bar'     => _x('Game', 'add new on admin bar'),
			'add_new'            => _x('Add New', 'Game'),
			'add_new_item'       => __('Add New Game'),
			'new_item'           => __('New Game'),
			'edit_item'          => __('Edit Game'),
			'view_item'          => __('View Game'),
			'all_items'          => __('All Games'),
			'search_items'       => __('Search Games'),
			'parent_item_colon'  => __(''),
			'not_found'          => __('No Games found.'),
			'not_found_in_trash' => __('No Games found in Trash.')
		);
		$a_post_type_game_args = array(
			'labels'             => $a_post_type_game_labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'games' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 6,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
		);
		register_post_type( 'games', $a_post_type_game_args );
		
		
		/*
			Custom Post Type. News
		*/
		$a_post_type_news_labels = array(
			'name'               => _x('News', 'post type general name'),
			'singular_name'      => _x('News', 'post type singular name'),
			'menu_name'          => _x('News', 'admin menu'),
			'name_admin_bar'     => _x('News', 'add new on admin bar'),
			'add_new'            => _x('Add New', 'News'),
			'add_new_item'       => __('Add New News'),
			'new_item'           => __('New News'),
			'edit_item'          => __('Edit News'),
			'view_item'          => __('View News'),
			'all_items'          => __('All News'),
			'search_items'       => __('Search News'),
			'parent_item_colon'  => __(''),
			'not_found'          => __('No News found.'),
			'not_found_in_trash' => __('No News found in Trash.')
		);
		$a_post_type_news_args = array(
			'labels'             => $a_post_type_news_labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'news' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 6,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
		);
		register_post_type( 'news', $a_post_type_news_args );
		
		
		/*
			Custom Post Type. Guides
		*/
		$a_post_type_game_labels = array(
			'name'               => _x('Guides', 'post type general name'),
			'singular_name'      => _x('Guide', 'post type singular name'),
			'menu_name'          => _x('Guides', 'admin menu'),
			'name_admin_bar'     => _x('Guide', 'add new on admin bar'),
			'add_new'            => _x('Add New', 'Guide'),
			'add_new_item'       => __('Add New Guide'),
			'new_item'           => __('New Guide'),
			'edit_item'          => __('Edit Guide'),
			'view_item'          => __('View Guide'),
			'all_items'          => __('All Guides'),
			'search_items'       => __('Search Guides'),
			'parent_item_colon'  => __(''),
			'not_found'          => __('No Guides found.'),
			'not_found_in_trash' => __('No Guides found in Trash.')
		);
		$a_post_type_game_args = array(
			'labels'             => $a_post_type_game_labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'guides' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 6,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
		);
		register_post_type( 'guides', $a_post_type_game_args );
		
		
		/*
			Custom Taxonomy for Games. Post Type Genres.
		*/
		$a_tax_genre_labels = array(
			'name'                       => _x( 'Genres', 'taxonomy general name' ),
			'singular_name'              => _x( 'Genre', 'taxonomy singular name' ),
			'search_items'               => __( 'Search Genres' ),
			'popular_items'              => __( 'Popular Genres' ),
			'all_items'                  => __( 'All Genres' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Genre' ),
			'update_item'                => __( 'Update Genre' ),
			'add_new_item'               => __( 'Add New Genre' ),
			'new_item_name'              => __( 'New Genre Name' ),
			'separate_items_with_commas' => __( 'Separate Genres with commas' ),
			'add_or_remove_items'        => __( 'Add or remove Genres' ),
			'choose_from_most_used'      => __( 'Choose from the most used Genres' ),
			'not_found'                  => __( 'No Genres found.' ),
			'menu_name'                  => __( 'Genres' ),
		);
		$a_tax_genre_args = array(
			'hierarchical'          => false,
			'labels'                => $a_tax_genre_labels,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'             => true,
			'rewrite'               => array( 'slug' => 'genres' ),
		);
		register_taxonomy( 'genres', 'games', $a_tax_genre_args );
		
		
		/*
			Custom Taxonomy for Games. Post Type Platforms.
		*/
		$a_tax_genre_labels = array(
			'name'                       => _x( 'Platforms', 'taxonomy general name' ),
			'singular_name'              => _x( 'Platform', 'taxonomy singular name' ),
			'search_items'               => __( 'Search Platforms' ),
			'popular_items'              => __( 'Popular Platforms' ),
			'all_items'                  => __( 'All Platforms' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Platform' ),
			'update_item'                => __( 'Update Platform' ),
			'add_new_item'               => __( 'Add New Platform' ),
			'new_item_name'              => __( 'New Platform Name' ),
			'separate_items_with_commas' => __( 'Separate Platforms with commas' ),
			'add_or_remove_items'        => __( 'Add or remove Platforms' ),
			'choose_from_most_used'      => __( 'Choose from the most used Platforms' ),
			'not_found'                  => __( 'No Platforms found.' ),
			'menu_name'                  => __( 'Platforms' ),
		);
		$a_tax_genre_args = array(
			'hierarchical'          => false,
			'labels'                => $a_tax_genre_labels,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'             => true,
			'rewrite'               => array( 'slug' => 'platforms' ),
		);
		register_taxonomy( 'platforms', 'games', $a_tax_genre_args );

		
	}
endif;
add_action('init', 'ggr_setup');


/**
 * hooks
 * 
 *
 **/

/*
	We still can't use this now as Sidebar gets affected
 
if( !function_exists( 'per_archive_basis' ) ):

	function per_archive_basis($query)
	{
		if( $query->is_archive ) 
		{
			if( is_archive('games') )
			{
				$query->set('posts_per_page', 1);
			}
		}
		return $query;
	}

endif;
add_filter('pre_get_posts', 'per_archive_basis');
*/ 


?>