## problems/questions/intimidations/thoughts
* what i need to know fast to remove the forgot it so i fear syndrome
** scan tutorialspoint.com an hour for this topic and you'll see its easy to understand as its just javascript
** nodejs is just a server side platform
** nodejs is the N in MEAN stack dev. MongoExpressAngularNode
** executing a file is as simple as $"node ____.js"
** basically it will execute what is inside the file you exec
** most of the time you include modules that you want to use. Eg. var http = require("http"); to create a server. Each of these modules are just objects that has callback functions that are easy to learn it if you need it.
** you can install other modules by "npm install _____". Eg. "npm install express" then use it inside your js like var express = require("express");
** nodejs comes with a bundled REPL enrivonment. Its just like a window console or git bash. You can write simple code in the terminal for testing. Eg. "1+1" or "console.log(...);". ctrl + c twice command to come out of Node.js REPL.
** nodejs comes also with a bundled npm. NPM provides 2 main func. Online repositories for node.js packages/modules which are searchable on search.nodejs.org and Command line utility to install Node.js packages, do version management and dependency management
of Node.js packages.
** you can check the version of nodejs by "node -v" command, i think. "npm -v" for npm
** so basically its a javascript file full of objects with callback functions. The callback functions is executed when needed. They say its fast because its asynchronous in its ways. Eg. The handler or listener is called but nodejs does not wait for its response as it will continue to call what needs to be call or event. There would be another guy responsible for the handling the responses of the called event or callback.
** want to create a website? Its like old time style from what i see. You create the html file in the dir, then write a code in the js to read the html file and outpud it. Wtf? So that's why there's express modules where it can help you in building webpage but after seeing express in action i feel kapoy gyapon bec of mag himo napud ka routing just like what we do on ci or cakephp routing
** express here and the expressjs are the same
** maybe if im intermediate at express and mongodb, i might find sense in this nodejs
* what nodejs can do really
** basically for now what i see is that it can do what other server scripting language can do. BUT, but how it does its thing (asynchronous or non blocking) is where it differs from the others (waterfall) ??
* what are the limitations of nodejs
** idk yet
* what are the requirements to setup nodejs
** IDK for now. Nodejs is just like an apache server. Since idk what apache server needs since i only fire up xampp and forgive and forget
* what are the steps to setup nodejs
** install nodejs first then thats it. You can use the terminal to run you script
* what are the pitfalls to note while setting and using nodejs
** before doing npm installs. I think you should do npm init first so that the modules will be installed inside your current project location
* it says, what are the best situation to use nodejs
** I/O bound Applications
** Data Streaming Applications
** Data Intensive Real time Applications (DIRT)
** JSON APIs based Applications
** Single Page Applications
* it says, what are NOT the best situation to use nodejs
** CPU intensive applications. Idk yet whats that
* for me, what are the advantages using nodejs
* for me, what are the disadvantages using nodejs