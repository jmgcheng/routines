/** php lesson {n} - declare variables */
	$txt = "Hello world!";

/** php lesson {n} - output variables */
	echo "I love $txt!";

/** php lesson {n} - global keyword */
	$x = 5;
	$y = 10;
	function myTest() {
		global $x, $y;
		$y = $x + $y;
	}
	myTest();
	echo $y; // outputs 15
	//or
	$x = 5;
	$y = 10;
	function myTest() {
	$GLOBALS['y'] = $GLOBALS['x'] + $GLOBALS['y'];
	} 
	myTest();
	echo $y; // outputs 15

/** php lesson {n} - static keyword */
	//static var will not be deleted after function for further exec
	function myTest() {
		static $x = 0;
		echo $x;
		$x++;
	}

/** php lesson {n} - create array */
	$cars = array("Volvo","BMW","Toyota");
	$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
	
/** php lesson {n} - create object */	
	$herbie = new Car();
	
/** php lesson {n} - constants */	
	define(name, value, case-insensitive)
	define("GREETING", "Welcome to W3Schools.com!");
	echo GREETING;

/** php lesson {n} - if elseif else statement */	
	if (condition) {} 
	elseif (condition) {} 
	else {}

/** php lesson {n} - switch statement */	
	switch (n) {
		case label1:
			break;
		...
		default:
			//else
	}

/** php lesson {n} - while loop */	
	while (condition is true) {
	}

/** php lesson {n} - do while */	
	do {
	} while (condition is true);

/** php lesson {n} - for */
	for (init counter; test counter; increment counter) {
	}

/** php lesson {n} - foreach */
	foreach ($array as $value) {
	}
	foreach($age as $x => $x_value) {
		echo "Key=" . $x . ", Value=" . $x_value;
		echo "<br>";
	}

/** php lesson {n} - include and require */
	include 'filename';
	require 'filename';

/** php lesson {n} - create/update cookies */
	setcookie(name, value, expire, path, domain, secure, httponly);
	setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
	
/** php lesson {n} - delete cookie */
	// set the expiration date to one hour ago
	setcookie("user", "", time() - 3600);

/** php lesson {n} - session */
	session_start();
	$_SESSION["favcolor"] = "yellow";
	print_r($_SESSION);
	// remove all session variables
	session_unset(); 
	// destroy the session 
	session_destroy(); 

/** php lesson {n} - filters */
	//http://www.w3schools.com/php/php_ref_filter.asp
	foreach (filter_list() as $id =>$filter) {
	echo '<tr><td>' . $filter . '</td><td>' . filter_id($filter) . '</td></tr>';
	}

	
	
	
	
	
	
/** php lesson {n} - create class */
	class person 
	{
		var $name;
		function set_name($new_name) 
		{
			$this->name = $new_name;
		}
		function get_name() 
		{
			return $this->name;
		} 
	}
	//
	$stefan = new person();
	$stefan->set_name("Stefan Mischook")
	echo "Stefan's full name: " . $stefan->get_name();

/** php lesson {n} - class - constructors */
	function __construct($persons_name) 
	{
		$this->name = $persons_name;            
	}

/** php lesson {n} - class - access modifiers */
	class person 
	{          
		var $name; //considered directly as public
		public $height;         
		protected $social_insurance;
		private $pinn_number;
		...
	}

/** php lesson {n} - class - inheritance */
	class employee extends person 
	{
		function __construct($employee_name) 
		{
			$this->set_name($employee_name);
		}
	}

/** php lesson {n} - class - overriding parent methods */
	//create a method with same name that do diff thing
	function set_name($new_name) 
	{
		if ($new_name == "Stefan Sucks") 
		{
			$this->name = $new_name;
		}
	}

/** php lesson {n} - class - overided but still use base method */
	function set_name($new_name) 
	{
		if ($new_name ==  "Stefan Sucks") 
		{
			$this->name = $new_name;
		}
		else if ($new_name ==  "Johnny Fingers") 
		{
			person::set_name($new_name);
		}
	}

/** php lesson {n} - class -  */
	













































































































































































































































	