<?php
	session_start();
	$b_signin = false;
	if( isset($_SESSION['b_signin']) && ($_SESSION['b_signin'] == true) )
	{
		$b_signin = true;
	}
?>
<html>
	<head>
		<title>Session Exercise</title>
	</head>
	<body>
		<h1>
			Index.php
		</h1>
		<?php
			if( $b_signin ):
		?>
			<h2>
				Welcome,
				<?php
					echo $_SESSION['s_username'];
				?>
			</h2>
			<p>
				You can now <a href="logout.php">Logout</a> now.
			</p>
		<?php
			else:
		?>
			<h2>
				Welcome Guest
			</h2>
			<p>
				You can either <a href="register.php">Register</a> or <a href="login.php">Login</a>
			</p>
		<?php
			endif;
		?>
	</body>
</html>