/** bower lesson {n} - install bower */
	- npm install -g bower

/** bower lesson {n} - install packages */
	//registered package
	- $ bower install jquery
	//GitHub shorthand
	- $ bower install desandro/masonry
	//Git endpoint
	- $ bower install git://github.com/user/package.git
	//URL
	- $ bower install http://example.com/script.js
	//
	- bower creates bower_components folder in root
	- this is where bower will put all of your installed packages
	- you do not need to upload this to your repo eg. git
		
/** bower lesson {n} - bower init */
	- $ bower init
	//
	- sets up bower.json
	- this is where the file dependency will be listed
	- you do not need to upload bower_components folder to repo
	- you only need this bower.json file so bower will now what to dl later

/** bower lesson {n} - use packages */
	//.html
	<script src="bower_components/jquery/dist/jquery.min.js"></script>

/** bower lesson {n} - init */	
	- $ bower init
	
/** bower lesson {n} - steps - new project - using git repo */
	- install bower if not yet installed
	- install packages you want to use using bower. eg. jquery
	- refer to use packages on how to use in you project
	- add gitignore file to ignore bower_components folder. So that all files inside that folder will not be sent to repo wasting space
	- setup - bower init - bower will create bower.json containing details of files your projects needs. eg jquery, bootstrap, etc
	- you can now push your files to repo with bower.json and without bower_components folder

/** bower lesson {n} - steps - existing project - using git repo */
	- pull the files in repo
	- run git
	- $ bower install
	- bower will check bower.json file for the files needed for this project
	- bower_components folder will be created and all packages needed for this project will be automatically downloaded 

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */

/** bower lesson {n} -  */