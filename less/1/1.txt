/** less lesson {n} - install */
	- install nodejs
	- run cmd
	- npm install -g less

/** less lesson {n} - cmd compile usage */
	run cmd
	lessc [option option=parameter ...] <source> [destination]
	lessc styles.less > styles.css
	lessc E:\xampp\htdocs\test1\test.less > E:\xampp\htdocs\test1\test.css

/** less lesson {n} - n++ setup - compile */
	create less file
	Run -> Run
	"path\lessc.cmd" "$(FULL_CURRENT_PATH)" > "$(CURRENT_DIRECTORY)\$(NAME_PART).css"
	"C:\Users\PW\AppData\Roaming\npm\lessc.cmd" "$(FULL_CURRENT_PATH)" > "$(CURRENT_DIRECTORY)\$(NAME_PART).css"

/** less lesson {n} - n++ setup - syntax highlight */
	http://notepad-plus.sourceforge.net/commun/userDefinedLang/less.xml
	run n++
	Language > Define Your Language > import xml file

/** less lesson {n} - variables */
	@grass: #aaaaaa;
	body
	{
		background-color:@grass;
	}

/** less lesson {n} - mixins */
	.panel{
		border: 1px solid dodgerblue;
		background: lightgreen;
		margin: 10px;
	}
	.little-panel{
		.panel;
		font-size: 12px;
		padding: 5px;
	}

/** less lesson {n} - nested rules */
	#side-nav{
		background: #333;
		a:link, a:visited{
			color: white;
			display: block;
			padding: 8px;
		}
		a:hover{
			background: #777;
		}
	}

/** less lesson {n} - nested conditionals */
	#welcome-baner{
		font-size: 32px;
		background: lightgreen;
		@media screen{
			@media (max-width:768px){
				font-size: 18px;
				background: lightsteelblue;
			}
		}
	}

/** less lesson {n} - simple operations */
	@buttonBackground: #006699;
	@buttonColor: #FFF;
	@buttonPadding: 5px;
	.button{
		background: @buttonBackground;
		color: @buttonColor;
		padding: @buttonPadding;
		display: inline-block;
	}
	.jumbo-botton{
		.button;
		padding: @buttonPadding + 30
	}

/** less lesson {n} - importing */
	@import "buttons"; /*buttons.less*/
	@import "forms"; /*forms.less*/

/** less lesson {n} - file paths */
	@images: "images/";
	@homepage-images: "images/homepage/";
	#one{
		height: 500px;
		width: 800px;
		margin-bottom: 10px;
		background: url("@{images}watermelon.jpg"); 
	}

/** less lesson {n} -  */

/** less lesson {n} -  */

/** less lesson {n} -  */

/** less lesson {n} -  */

/** less lesson {n} -  */

/** less lesson {n} -  */

/** less lesson {n} -  */

/** less lesson {n} -  */

/** less lesson {n} -  */

/** less lesson {n} -  */

/** less lesson {n} -  */

/** less lesson {n} -  */

/** less lesson {n} -  */