/** sass {n} - install */
	- install Ruby
	- run cmd
		- gem install sass 
		- sudo gem install sass //OR 

/** sass {n} - documentation */		
	--help

/** sass {n} - cmd compile usage */
	run cmd
	sass input.scss output.css
	//You can also tell Sass to watch the file and update the CSS every time the Sass file changes:
	sass --watch input.scss:output.css
	//If you have a directory with many Sass files, you can also tell Sass to watch the entire directory:
	sass --watch app/sass:public/stylesheets

/** sass {n} - variables */
	//sass
	$font-stack:    Helvetica, sans-serif
	$primary-color: #333
	body
	  font: 100% $font-stack
	  color: $primary-color
	//SCSS
	$font-stack:    Helvetica, sans-serif;
	$primary-color: #333;
	body {
	  font: 100% $font-stack;
	  color: $primary-color;
	}

/** sass {n} - nesting */
	//sass
	nav
	  ul
		margin: 0
		padding: 0
		list-style: none
	  li
		display: inline-block
	//SCSS
	nav {
	  ul {
		margin: 0;
		padding: 0;
		list-style: none;
	  }
	  li { display: inline-block; }
	
/** sass {n} - Partials - Import */	
	//sass
	@import reset
	//SCSS
	@import 'reset';

/** sass {n} - mixins */
	//sass
	=border-radius($radius)
		-webkit-border-radius: $radius
		-moz-border-radius:    $radius
		-ms-border-radius:     $radius
		border-radius:         $radius
	.box
		+border-radius(10px)
	//SCSS
	@mixin border-radius($radius) {
	  -webkit-border-radius: $radius;
		 -moz-border-radius: $radius;
		  -ms-border-radius: $radius;
			  border-radius: $radius;
	}
	.box { @include border-radius(10px); }

/** sass {n} - Extend/Inheritance */	
	//sass
	.message
	border: 1px solid #ccc
	padding: 10px
	color: #333
	.success
	@extend .message
	border-color: green
	//SCSS
	.message {
		border: 1px solid #ccc;
		padding: 10px;
		color: #333;
	}
	.success {
		@extend .message;
		border-color: green;
	}
	
/** sass {n} - operators */	
	//sass
	.container
	  width: 100%
	article[role="main"]
	  float: left
	  width: 600px / 960px * 100%

	aside[role="complimentary"]
	  float: right
	  width: 300px / 960px * 100%
	//SCSS
	.container { width: 100%; }
	article[role="main"] {
	  float: left;
	  width: 600px / 960px * 100%;
	}
	aside[role="complimentary"] {
	  float: right;
	  width: 300px / 960px * 100%;
	}
	
	
/** sass {n} -  */

/** sass {n} -  */

/** sass {n} -  */

/** sass {n} -  */

/** sass {n} -  */

/** sass {n} -  */

/** sass {n} -  */

/** sass {n} -  */

/** sass {n} -  */

/** sass {n} -  */

/** sass {n} -  */

/** sass {n} -  */

/** sass {n} -  */