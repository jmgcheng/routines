## problems/questions/intimidations/thoughts
* what i need to know fast to remove the forgot it so i fear syndrome
** scan tutorialspoint.com an hour for this topic and you'll see its easy to understand as its just javascript
** express is the E in MEAN stack dev. MongoExpressAngularNode
** express is just a web application framework you can use if your using nodejs. Like expressJs=ci;nodeJs=apache
** its just like codeigniter really 
** like nodejs, all code you do in express are inside .js and with a syntax of javascript
* what expressjs can do really
** like how ci helps you when developing using php, express helps you shorten your code while developing inside nodejs
** please note that express is a web app framework, not a templating engine. Some of the templating engine you can use inside express are Pug or Jade which is fairly easy to undertand if you use sublime shortcuts
** like any other modules in nodejs, you need to install express before you can use it by "npm install --save express". The --save flag can be replaced by -S flag. This flag ensures that express is added as a dependency
to our package.json file. This has an advantage, the next time we need to install all the dependencies of
our project we just need to run the command npm install and it’ll find the dependencies in this file and
install them for us.
* what are the limitations of expressjs
* what are the requirements to setup expressjs
** nodejs 
** npm
* what are the steps to setup expressjs
** npm install --save express
* what are the pitfalls to note while setting and using expressjs
** please note that express is a web app framework, not a templating engine. Some of the templating engine you can use inside express are Pug or Jade which is fairly easy to undertand if you use sublime shortcuts
** Unlike Django, rails, or maybe cakePhp which have a defined way of doing things, file structure, etc, Express is unopinionated on this. What this means is you can structure the application however you like. But as your application grows in size, its very difficult to maintain it if it doesn't have a well defined structure.
* for me, what are the best situation to use expressjs
** maybe when you use nodejs and mongodb then your sure you are going to use express to help you on the web side
* for me, what are NOT the best situation to use expressjs
** its just that mongoDb and angularJs can be used in php where in php you can use cake or ci or wordpress or etc. I just dont get it why you need to transfer and use nodeJs and expressJs where for me its just bare... Maybe i'll understand more if i get more geekier
* for me, what are the advantages using expressjs
** this is part of the MEAN stack. So the advantage of using MEAN stack are the advantages. Read nodeJs readme
* for me, what are the disadvantages using expressjs
** this is part of the MEAN stack. So the disAdvantage of using MEAN stack are the disAdvantage. Read nodeJs readme