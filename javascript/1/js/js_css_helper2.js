﻿window.onload = function(){
	
	o_user = {
		s_navigator_useragent: '',
		s_navigator_useragent_browser_name: '',
		s_navigator_useragent_browser_version: '',
		
		i_supported_screensizes: new Array('768', '720', '640', '540', '480', '320'),
		s_css_screensize: '',
		
		set_navigator_useragent: function(){
			var s_navigator_useragent = navigator.userAgent.toLowerCase();
			o_user.s_navigator_useragent = s_navigator_useragent;
			
			if(s_navigator_useragent.indexOf("msie") != -1) 
			{
				o_user.s_navigator_useragent_browser_name = 'msie';
				
				var s_version = String( s_navigator_useragent.match(/msie \d*/g) );
				o_user.s_navigator_useragent_browser_version = s_version.replace(' ', '_v');
			}
			else if(s_navigator_useragent.indexOf("chrome") != -1)
			{
				o_user.s_navigator_useragent_browser_name = 'chrome';
				
				var s_version = String( s_navigator_useragent.match(/chrome\/\d*/g) );
				o_user.s_navigator_useragent_browser_version = s_version.replace('/', '_v');
			}
			else if(s_navigator_useragent.indexOf("firefox") != -1) 
			{
				o_user.s_navigator_useragent_browser_name = 'firefox';
				
				var s_version = String( s_navigator_useragent.match(/firefox\/\d*/g) );
				o_user.s_navigator_useragent_browser_version = s_version.replace('/', '_v');
			}
		},
		
		add_class_navigator_useragent_details: function(){
			/*
			$("body").addClass( o_user.s_navigator_useragent_browser_name );
			$("body").addClass( o_user.s_navigator_useragent_browser_version );
			*/
			document.getElementsByTagName("body")[0].className += ' ' + o_user.s_navigator_useragent_browser_name;
			document.getElementsByTagName("body")[0].className += ' ' + o_user.s_navigator_useragent_browser_version;
		},
		
		show_navigator_useragent_details: function(){
			console.log( o_user );
			/*
			$('body').append( o_user.s_navigator_useragent + '<br/>' );
			$('body').append( o_user.s_navigator_useragent_browser_name + '<br/>' );
			$('body').append( o_user.s_navigator_useragent_browser_version + '<br/>' );
			*/
		},
		
		set_screensize: function(){
			s_temp_css_screensize = 'screensize_default';

			o_user.i_supported_screensizes.sort();
			for(i_counter = 0; i_counter < o_user.i_supported_screensizes.length; i_counter++) 
			{ 
				//if( $(window).width() <= o_user.i_supported_screensizes[i_counter] )
				if( document.documentElement.clientWidth <= o_user.i_supported_screensizes[i_counter] )
				{
					s_temp_css_screensize = 'screensize_' + o_user.i_supported_screensizes[i_counter];
					break;
				}
			}
			
			/*
			alert($(window).width());
			alert(document.documentElement.clientWidth);
			alert(o_user.s_css_screensize);
			*/
			
			o_user.s_css_screensize = s_temp_css_screensize;

			/*
			$("body").attr("screensize_helper", o_user.s_css_screensize);
			*/
			document.getElementsByTagName("body")[0].setAttribute("screensize_helper", o_user.s_css_screensize);
		}
		
	};
	
	
	o_user.set_navigator_useragent();
	o_user.set_screensize();
	//o_user.show_navigator_useragent_details();
	
	o_user.add_class_navigator_useragent_details();

	/*
	$( window ).resize(function() {
		o_user.set_screensize();
	});
	*/
	window.onresize = function(event) {
	   o_user.set_screensize();
	};
};