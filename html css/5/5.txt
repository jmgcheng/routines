/** css lesson {n} - simple menu */
	ul li
	{
		position:relative;
		float:left; //situational
	}
	ul li a
	{
		display:block;
	}
	ul li ul
	{
		display:none;
		position:absolute;
		margin:0px; //situational
		top:100%; //situational
		right:0px; //situational
	}
	ul ul ul
	{
		top:0; //situational
		right:100% //situational
	}
	ul li:hover > ul
	{
		display:block;
	}

	
	
	
	
/** css lesson {n} - syntax */
	___ {___:___; ___:___}
	selector {declaration; declaration}
	selector {property:value; property:value}
	h1 {color:blue; font-size:12px;}

/** css lesson {n} - grouping */
	h1, h2, p {
		text-align: center;
		color: red;
	}
	
/** css lesson {n} - sans-serif vs serif vs monospace */
	serifs has tails
	monospace. All characters have the same width
	
/** css lesson {n} - box model */
	margin
		border
			padding
				content
	
/** css lesson {n} - shorthand - background */
	background: #ffffff url("img.jpeg") no-repeat right top;

/** css lesson {n} - shorthand - font */
	font: font-style font-variant font-weight font-size/line-height font-family;
	font: italic small-caps normal 13px/150% Arial, Helvetica, sans-serif;
	
/** css lesson {n} - shorthand - border */
	border: 5px solid red;
	
/** css lesson {n} - shorthand - margin/padding */
	__: 100px 50px;
	__: 100px 50px 100px 50px;

/** css lesson {n} - font fallback system */
	font-family: "Times New Roman", Times, serif;
	http://www.w3schools.com/cssref/css_websafe_fonts.asp

/** css lesson {n} - anchor important order - LoVe HAte */
	a:link
	a:visited
	a:hover
	a:active		A link becomes active WHEN YOU CLICK IT
	
/** css lesson {n} - dimension */
	height		sets height of an element
	width		sets width of an element
	max-height	sets maximum height
	max-width	sets maximum width
	min-height	sets minimum height
	min-width	sets minimum width
	
/** css lesson {n} - opacity */	
	img {
		opacity: 0.4;
		filter: alpha(opacity=40); /* For IE8 and earlier */
	}
	
/** css lesson {n} - position & z-index */
	:static
	:fixed
	:relative
	:absolute
	z-index 	specifies the stack order of an element

/** css lesson {n} - clear float */
	clear: both;
	<h3 class="clear_float">Second row</h3>
	<div style="clear: both;"></div>


/** css lesson {n} - attribute selectors */
	http://www.w3schools.com/css/css_attribute_selectors.asp
	[attribute]
	a[target] {}			<a> elements with a target attribute
	[attribute=value]
	a[target="_blank"] {}	<a> elements with a target="_blank" attribute
	[attribute~=value]
	[title~="flower"] {}	elements with an attribute value containing a specified word
	[attribute|=value]
	[class|="top"] {}		elements with a class attribute value that begins with "top"
	[attribute^=value]
	[class^="top"] {}		elements whose attribute value begins with a specified value
	[attribute$=value]
	[class$="test"]	{}		elements whose attribute value ends with a specified value
	[attribute*=value]
	[class*="te"] {}		elements whose attribute value contains a specified value

	
/** css lesson {n} - combinators */
	descendant 			- all p inside div
		div p
	child				- all p inside div but not inside another element
		div > p
	adjacent sibling	- next single p
		div + p
	general sibling		- next all p
		div ~ p
	http://www.w3schools.com/css/css_combinators.asp
	
/** css lesson {n} - pseudo classes */
	selector:pseudo-class {
		property:value;
	}
	http://www.w3schools.com/css/css_pseudo_classes.asp
	p:first-child		match first p of any element
	p i:first-child		match first i of any p element
	p:first-child i		match i of the first p element
	p:first-of-type		match first type of p
	p:last-child		match last p
	p:last-of-type		match last type of p
	:not(p)				match elements that are not p
	p:nth-child(2)		match p that is the 2nd child of its parent
	p:nth-last-child(2)	reverse count of nth-child(x)
	p:nth-of-type(2)	match p that is the 2nd p of its parent
	p:only-of-type		match p that is the only child of its type, of its parent
	p:only-child		match p that is the only child of its parent
	input:focus			when element is in focus
	input:in-range		style only if input value "in range"
						<input type="number" min="5" max="10" />
	input:out-of-range	style only if input value "out of range"
	input:invalid		match invalid input
						<input type="email" value="supportEmail" />
	input:valid			match valid input
	input:optional		match input does not have a "required" attribute
	input:read-only		match input element is "readonly"
	input:-moz-read-only
	input:read-write	match input element is NOT "readonly"
	input:-moz-read-write
	input:required		match input required
	Set the background color for the HTML document:
	:root { 			
		background: #ff0000;
	}
	Highlight active HTML anchor:
	:target {			
		border: 2px solid #D4D4D4;
		background-color: #e5eecc;
	}
	<p><a href="#news1">Jump to New content 1</a></p>
	<p id="news1"><b>New content 1...</b></p>
	
/** css lesson {n} - :lang Pseudo-class */
	define special rules for different languages.
	q:lang(no) {
		quotes: "~" "~";
	}
	<p>Some text <q lang="no">A quote in a paragraph</q> Some text.</p>
	:lang(en){
		font-style: italic;
	}
	<p>The man said <q lang="en">The quote is input here</q>.</p>
	
/** css lesson {n} - pseudo elements */	
	selector::pseudo-element {
		property:value;
	}	
	Insert content after every <p> element:
	p::after { 
		content: " - Remember this";
	}
	Insert content before every <p> element's content:
	p::before { 
		content: "Read this: ";
	}
	Select and style the first letter of every <p> element:
	p::first-letter { 
		font-size: 200%;
		color: #8A2BE2;
	}
	Select and style the first line of every <p> element:
	p::first-line { 
		background-color: yellow;
	}
	Make the selected text red on a yellow background: "Highlight"
	::-moz-selection {
		color: red;
		background: yellow;
	}
	::selection {
		color: red; 
		background: yellow;
	}
	
/** css lesson {n} - media types */
	@media Rule
	@media screen { }		Used for computer screens
	@media print { }		Used for printers
	@media handheld { }		Used for small or handheld devices
	@media tv { }			Used for television-type devices
	
	
	



	
/** css lesson {n} - border-radius */
	border-radius: 25px;
	border-radius: 15px 50px 30px 5px;
	-webkit-				old chrome & safari
	-moz-					old mozilla
	
/** css lesson {n} - border-image */
	-webkit-border-image: url(border.png) 50 round; /* Safari 3.1-5 */
    -o-border-image: url(border.png) 50 round; /* Opera 11-12.1 */
    border-image: url(border.png) 50 round;
	
/** css lesson {n} - multiple backgrounds */
	#example1 {
		background-image: url(img_flwr.gif), url(paper.gif);
		background-position: right bottom, left top;
		background-repeat: no-repeat, repeat;
	}
	#example2 {
		background: url(img_flwr.gif) right bottom no-repeat, 
					url(paper.gif) left top repeat;
	}
	
/** css lesson {n} - background size */	
	background-size: 100px 80px;
	background-size: contain;	scales bg to be as large as possible
	background-size: cover;		scales bg so that the content area is 
								completely covered by the background image
	#example1 {
		background: url(img_flwr.gif) left top no-repeat, 
					url(img_flwr.gif) right bottom no-repeat, 
					url(paper.gif) left top repeat;
		background-size: 50px, 130px, auto;
	}							
								
/** css lesson {n} - background origin */
	specifies where the background image is positioned
	border-box		starts from the upper left corner of the border
	padding-box		(default). starts from the upper left corner of	the padding edge
	content-box		starts from the upper left corner of the content
	
/** css lesson {n} - background clip */
	specifies the painting area of the background
	border-box		painted to the outside edge of the border
	padding-box		painted to the outside edge of the padding
	content-box		painted within the content box
	
/** css lesson {n} - gradients linear */
	background: linear-gradient(direction, color-stop1, color-stop2, ...);
	#grad {
		background: -webkit-linear-gradient(red, blue);
		background: -o-linear-gradient(red, blue);
		background: -moz-linear-gradient(red, blue);
		background: linear-gradient(red, blue);
	}
		
/** css lesson {n} - gradients radial */
	background: radial-gradient(shape size at position, start-color, ..., last-color);
	#grad {
		background: radial-gradient(red, green, blue);
	}
	
/** css lesson {n} - text shadow */	
	text-shadow: 2px 2px;
	text-shadow: 2px 2px red;
	text-shadow: 2px 2px 5px red;	//blur
	text-shadow: 0 0 3px #FF0000;	//neon
	
/** css lesson {n} - box shadow */	
	box-shadow: 10px 10px;
	box-shadow: 10px 10px grey;
	box-shadow: 10px 10px 5px grey;	//blur
	
/** css lesson {n} - text overflow */	
	text-overflow: clip;			//cut
	text-overflow: ellipsis;		//...
	
/** css lesson {n} - Word Wrapping */	
	word-wrap: break-word;
	
/** css lesson {n} - Word Breaking */	
	word-break: keep-all;
	word-break: break-all;
	
/** css lesson {n} - Custom Font Face */
	@font-face {
		font-family: myFirstFont;
		src: url(sansation_light.woff);
	}
	div {
		font-family: myFirstFont;
	}
	@font-face {
		font-family: myFirstFont;
		src: url(sansation_bold.woff);
		font-weight: bold;
	}	
	
/** css lesson {n} - 2D Transform - translate */	
	moves an element from its current position
	transform: translate(50px,100px);

/** css lesson {n} - 2D Transform - rotate */	
	transform: rotate(20deg);
	
/** css lesson {n} - 2D Transform - scale */	
	transform: scale(2,3);

/** css lesson {n} - 2D Transform - skew */	
	transform: skewX(20deg);
	transform: skewY(20deg);
	transform: skew(20deg, 10deg);
	
/** css lesson {n} - 2D Transform - matrix */	
	combines all the 2D transform methods
	transform: matrix(1, -0.3, 0, 1, 0, 0);
	
/** css lesson {n} - 3D Transform - rotate */	
	transform: rotateX(150deg);
	transform: rotateY(130deg);
	transform: rotateZ(90deg);
	
/** css lesson {n} - Transitions */	
	div {
		width: 100px;
		height: 100px;
		transition: width 2s;
	}
	div:hover {
		width: 300px;
	}
	transition: width 2s, height 4s;
	
/** css lesson {n} - Transitions + Transform */	
	div {
	width: 100px;
	height: 100px;
	background: red;
	-webkit-transition: width 2s, height 2s, -webkit-transform 2s;
	transition: width 2s, height 2s, transform 2s;
	}
	div:hover {
	width: 300px;
	height: 300px;
	-webkit-transform: rotate(180deg);
	transform: rotate(180deg);
	}
	
/** css lesson {n} - Animations */		
	/* The animation code */
		@keyframes example {
		from {background-color: red;}
		to {background-color: yellow;}
		}
	/* The element to apply the animation to */
		div {
		width: 100px;
		height: 100px;
		background-color: red;
		animation-name: example;
		animation-duration: 4s;
		}
	//shorthand
	div {
		animation: example 5s linear 2s infinite alternate;
	}
	
/** css lesson {n} - Multi Columns Layout */	
	column-count: 3;
	
/** css lesson {n} - Resizing */		
	resize: horizontal; //vertical, both
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	